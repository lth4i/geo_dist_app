data = {'ap-northeast-1': {'ap-northeast-1': u"{'bw_mean': 17.178, 'bw_std': 7.6706203138990015, 'rtt_mean': 1.075, 'rtt_std': 0.206}",
   'ap-southeast-1': u"{'bw_mean': 3.0360000000000005, 'bw_std': 0.7757989430258332, 'rtt_mean': 92.165, 'rtt_std': 2.678}",
   'ap-southeast-2': u"{'bw_mean': 3.0100000000000002, 'bw_std': 0.5557697364916516, 'rtt_mean': 136.052, 'rtt_std': 2.07}",
   'eu-west-1': u"{'bw_mean': 1.786, 'bw_std': 0.7380677475679316, 'rtt_mean': 280.399, 'rtt_std': 4.224}",
   'sa-east-1': u"{'bw_mean': 0.774, 'bw_std': 0.2806136133547338, 'rtt_mean': 366.418, 'rtt_std': 0.416}",
   'us-east-1': u"{'bw_mean': 2.676, 'bw_std': 0.2778920653779089, 'rtt_mean': 165.646, 'rtt_std': 0.575}",
   'us-west-1': u"{'bw_mean': 2.2, 'bw_std': 0.20600970850908948, 'rtt_mean': 105.019, 'rtt_std': 1.811}",
   'us-west-2': u"{'bw_mean': 2.46, 'bw_std': 0.17285832349065525, 'rtt_mean': 108.314, 'rtt_std': 5.591}"},
  'ap-southeast-1': {'ap-northeast-1': u"{'bw_mean': 2.5740000000000003, 'bw_std': 0.3467333269243094, 'rtt_mean': 93.101, 'rtt_std': 3.524}",
   'ap-southeast-1': u"{'bw_mean': 17.204, 'bw_std': 7.209911511246167, 'rtt_mean': 3.298, 'rtt_std': 4.447}",
   'ap-southeast-2': u"{'bw_mean': 1.3639999999999999, 'bw_std': 0.1298614646459834, 'rtt_mean': 187.319, 'rtt_std': 4.744}",
   'eu-west-1': u"{'bw_mean': 2.0759999999999996, 'bw_std': 0.8035322022171856, 'rtt_mean': 198.488, 'rtt_std': 0.592}",
   'sa-east-1': u"{'bw_mean': 0.5740000000000001, 'bw_std': 0.3181571938523472, 'rtt_mean': 369.222, 'rtt_std': 1.696}",
   'us-east-1': u"{'bw_mean': 1.4360000000000002, 'bw_std': 0.6456190827415187, 'rtt_mean': 260.062, 'rtt_std': 3.289}",
   'us-west-1': u"{'bw_mean': 1.238, 'bw_std': 0.26415147169758496, 'rtt_mean': 180.38, 'rtt_std': 6.106}",
   'us-west-2': u"{'bw_mean': 0.914, 'bw_std': 0.10650821564555481, 'rtt_mean': 204.702, 'rtt_std': 4.842}"},
  'ap-southeast-2': {'ap-northeast-1': u"{'bw_mean': 3.7, 'bw_std': 0.5747347214150195, 'rtt_mean': 135.267, 'rtt_std': 2.228}",
   'ap-southeast-1': u"{'bw_mean': 3.3259999999999996, 'bw_std': 0.8674237718670152, 'rtt_mean': 187.921, 'rtt_std': 5.302}",
   'ap-southeast-2': u"{'bw_mean': 16.75, 'bw_std': 9.370242259408238, 'rtt_mean': 3.087, 'rtt_std': 6.523}",
   'eu-west-1': u"{'bw_mean': 1.6759999999999997, 'bw_std': 0.7926815249518561, 'rtt_mean': 335.268, 'rtt_std': 14.63}",
   'sa-east-1': u"{'bw_mean': 2.038, 'bw_std': 1.1338677171522258, 'rtt_mean': 336.497, 'rtt_std': 0.912}",
   'us-east-1': u"{'bw_mean': 2.974, 'bw_std': 1.1548090751288718, 'rtt_mean': 358.777, 'rtt_std': 2.925}",
   'us-west-1': u"{'bw_mean': 2.876, 'bw_std': 1.1377099806189626, 'rtt_mean': 187.042, 'rtt_std': 4.839}",
   'us-west-2': u"{'bw_mean': 3.088, 'bw_std': 0.6323416797902855, 'rtt_mean': 214.69, 'rtt_std': 0.342}"},
  'eu-west-1': {'ap-northeast-1': u"{'bw_mean': 1.998, 'bw_std': 0.7679166621450534, 'rtt_mean': 278.232, 'rtt_std': 0.503}",
   'ap-southeast-1': u"{'bw_mean': 1.06, 'bw_std': 0.06572670690061999, 'rtt_mean': 198.76, 'rtt_std': 0.531}",
   'ap-southeast-2': u"{'bw_mean': 1.026, 'bw_std': 0.37376998274339795, 'rtt_mean': 320.733, 'rtt_std': 23.746}",
   'eu-west-1': u"{'bw_mean': 17.204, 'bw_std': 7.601183065812847, 'rtt_mean': 2.274, 'rtt_std': 2.459}",
   'sa-east-1': u"{'bw_mean': 1.738, 'bw_std': 0.5142528560931868, 'rtt_mean': 231.309, 'rtt_std': 0.558}",
   'us-east-1': u"{'bw_mean': 2.1980000000000004, 'bw_std': 0.19903768487399565, 'rtt_mean': 95.535, 'rtt_std': 6.942}",
   'us-west-1': u"{'bw_mean': 1.9140000000000001, 'bw_std': 0.2011566553708825, 'rtt_mean': 169.779, 'rtt_std': 2.633}",
   'us-west-2': u"{'bw_mean': 2.012, 'bw_std': 0.29647259569815215, 'rtt_mean': 171.795, 'rtt_std': 5.977}"},
  'sa-east-1': {'ap-northeast-1': u"{'bw_mean': 0.036, 'bw_std': 0.029393876913398134, 'rtt_mean': 367.672, 'rtt_std': 3.696}",
   'ap-southeast-1': u"{'bw_mean': 0.024, 'bw_std': 0.029393876913398138, 'rtt_mean': 368.766, 'rtt_std': 0.806}",
   'ap-southeast-2': u"{'bw_mean': 1.21, 'bw_std': 0.5906606470724116, 'rtt_mean': 335.855, 'rtt_std': 0.503}",
   'eu-west-1': u"{'bw_mean': 2.624, 'bw_std': 1.0829699903506098, 'rtt_mean': 231.261, 'rtt_std': 0.282}",
   'sa-east-1': u"{'bw_mean': 16.786, 'bw_std': 8.614382392255408, 'rtt_mean': 4.12, 'rtt_std': 9.685}",
   'us-east-1': u"{'bw_mean': 6.686, 'bw_std': 1.503603671184664, 'rtt_mean': 132.358, 'rtt_std': 0.492}",
   'us-west-1': u"{'bw_mean': 1.4860000000000002, 'bw_std': 0.21886982432487123, 'rtt_mean': 185.525, 'rtt_std': 1.326}",
   'us-west-2': u"{'bw_mean': 0.174, 'bw_std': 0.07445804187594514, 'rtt_mean': 223.11, 'rtt_std': 6.889}"},
  'us-east-1': {'ap-northeast-1': u"{'bw_mean': 1.626, 'bw_std': 0.15435025105259786, 'rtt_mean': 165.701, 'rtt_std': 0.534}",
   'ap-southeast-1': u"{'bw_mean': 0.976, 'bw_std': 0.047999999999999994, 'rtt_mean': 258.252, 'rtt_std': 0.179}",
   'ap-southeast-2': u"{'bw_mean': 0.75, 'bw_std': 0.3014630989026683, 'rtt_mean': 356.226, 'rtt_std': 2.521}",
   'eu-west-1': u"{'bw_mean': 2.8, 'bw_std': 0.3431617694324354, 'rtt_mean': 90.893, 'rtt_std': 0.437}",
   'sa-east-1': u"{'bw_mean': 3.29, 'bw_std': 2.2199819819088624, 'rtt_mean': 132.401, 'rtt_std': 0.172}",
   'us-east-1': u"{'bw_mean': 48.64, 'bw_std': 8.060918061858711, 'rtt_mean': 1.09, 'rtt_std': 0.119}",
   'us-west-1': u"{'bw_mean': 2.85, 'bw_std': 0.3547957158704146, 'rtt_mean': 78.226, 'rtt_std': 0.268}",
   'us-west-2': u"{'bw_mean': 1.1640000000000001, 'bw_std': 0.2417933001553186, 'rtt_mean': 87.622, 'rtt_std': 0.261}"},
  'us-west-1': {'ap-northeast-1': u"{'bw_mean': 1.95, 'bw_std': 0.6443601477434804, 'rtt_mean': 105.971, 'rtt_std': 2.828}",
   'ap-southeast-1': u"{'bw_mean': 2.138, 'bw_std': 0.5731457057328442, 'rtt_mean': 179.585, 'rtt_std': 6.91}",
   'ap-southeast-2': u"{'bw_mean': 5.39, 'bw_std': 1.830234957594243, 'rtt_mean': 186.476, 'rtt_std': 3.46}",
   'eu-west-1': u"{'bw_mean': 2.9639999999999995, 'bw_std': 0.06118823416311347, 'rtt_mean': 169.288, 'rtt_std': 1.528}",
   'sa-east-1': u"{'bw_mean': 1.124, 'bw_std': 0.16414627622946548, 'rtt_mean': 185.249, 'rtt_std': 1.254}",
   'us-east-1': u"{'bw_mean': 3.25, 'bw_std': 2.3560050933731023, 'rtt_mean': 79.31, 'rtt_std': 2.232}",
   'us-west-1': u"{'bw_mean': 16.712, 'bw_std': 8.027120031493236, 'rtt_mean': 3.36, 'rtt_std': 6.893}",
   'us-west-2': u"{'bw_mean': 4.726000000000001, 'bw_std': 1.3887490774074345, 'rtt_mean': 31.917, 'rtt_std': 0.199}"},
  'us-west-2': {'ap-northeast-1': u"{'bw_mean': 3.9, 'bw_std': 0.5884895920914829, 'rtt_mean': 105.611, 'rtt_std': 0.364}",
   'ap-southeast-1': u"{'bw_mean': 4.45, 'bw_std': 1.7298670469142996, 'rtt_mean': 200.508, 'rtt_std': 2.729}",
   'ap-southeast-2': u"{'bw_mean': 2.372, 'bw_std': 0.847169404546694, 'rtt_mean': 370.689, 'rtt_std': 3.013}",
   'eu-west-1': u"{'bw_mean': 2.276, 'bw_std': 0.7417978161197295, 'rtt_mean': 174.173, 'rtt_std': 0.566}",
   'sa-east-1': u"{'bw_mean': 1.6380000000000003, 'bw_std': 0.5399036951160827, 'rtt_mean': 222.9, 'rtt_std': 4.078}",
   'us-east-1': u"{'bw_mean': 3.026, 'bw_std': 0.11516944039110376, 'rtt_mean': 84.801, 'rtt_std': 0.39}",
   'us-west-1': u"{'bw_mean': 9.187999999999999, 'bw_std': 0.43078532936951314, 'rtt_mean': 31.853, 'rtt_std': 1.153}",
   'us-west-2': u"{'bw_mean': 16.79, 'bw_std': 8.699908045491055, 'rtt_mean': 3.873, 'rtt_std': 8.327}"}}

import re
class Distance:
	def __init__(self, source, dest, data):
		self.source = source
		self.dest = dest
		self.data = data

	def __str__(self):
		return '{0} -> {1}: {2}'.format(self.source,
			self.dest, self.data)

	def __repr__(self):
		return self.__str__()

def parse_data_string(raw_data):
	data = filter(None, re.split(': |, |\'|\}|\{', raw_data))
	counter = 0
	result = {}
	while counter < len(data):
		result[data[counter]] = round(float(data[counter + 1]), 3) # assume that value a numeric value
		counter = counter + 2
	return result

def parse_data(filter=None):
	distances = {}
	for source in data:
		distances[source] = {}
		for dest in data[source]:
			full_data = parse_data_string(data[source][dest])
			if filter == None:
				distances[source][dest] = full_data
			else:
				distances[source][dest] = full_data[filter]
	return distances

def compare_rank():
  bw = parse_data('bw_mean')
  rtt = parse_data('rtt_mean')

  costs = {}
  for s in bw:
    if s not in costs: costs[s] = {}
    for d in bw:
      costs[s][d] = 1 / bw[s][d]

  rev_costs = reverse_data(costs)
  rev_rtt = reverse_data(rtt)

  sorted_costs = sorted(rev_costs.keys())
  sorted_rtt= sorted(rev_rtt.keys())

  tmp = []
  for v in sorted_rtt:
    tmp.extend(rev_rtt[v])
  count = 0
  for v in sorted_costs:
    for n in rev_costs[v]:
      print n, '\t', tmp[count], '\t', n == tmp[count]
      count = count + 1


def reverse_data(_data):
  res = {}
  for s in _data:
    for d in _data[s]:
      val = _data[s][d]
      if val not in res: res[val] = []
      res[val].append('{0} --> {1}'.format(s, d))

  return res

