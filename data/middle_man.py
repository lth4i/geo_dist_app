import matplotlib.pyplot as plt

from metric_collector import stats

hosts=[
	{
	u'54.193.112.103': 'us-west-1',
	u'54.199.227.131': 'ap-northeast-1',
	u'54.206.69.43': 'ap-southeast-2',
	u'54.207.63.23': 'sa-east-1',
	u'54.213.251.116': 'us-west-2',
	u'54.254.250.179': 'ap-southeast-1',
	u'54.72.21.251': 'eu-west-1',
	u'67.202.24.73': 'us-east-1'
	},
	{
	u'50.17.49.86': 'us-east-1',
	u'54.186.22.144': 'us-west-2',
	u'54.193.2.247': 'us-west-1',
	u'54.199.134.224': 'ap-northeast-1',
	u'54.206.10.48': 'ap-southeast-2',
	u'54.207.66.95': 'sa-east-1',
	u'54.254.253.192': 'ap-southeast-1',
	u'54.72.34.150': 'eu-west-1'
	},
	{
	u'46.51.199.171': 'eu-west-1',
	u'54.193.112.166': 'us-west-1',
	u'54.199.164.80': 'ap-northeast-1',
	u'54.206.90.1': 'ap-southeast-2',
	u'54.207.62.101': 'sa-east-1',
	u'54.213.149.153': 'us-west-2',
	u'54.254.176.204': 'ap-southeast-1',
	u'54.81.146.203': 'us-east-1'
	}
	]

def get_region(ip):
	for h in hosts:
		if ip in h: return h[ip]
	raise Exception('Unknown ip {0}'.format(ip))

def parse_result(name='data/middle_man'):
	with open(name) as f:
		lines = f.readlines()

	results = {}
	size = 0.0
	for line in lines:
		if line.strip() == '':
			continue
		elif line.startswith('python'):
			size = float(line.split()[8]) / 1024**2
			results[size] = {}
			results[size]['eu-west-1'] = []
			results[size]['ap-southeast-1'] = []
			results[size]['sa-east-1'] = []
			results[size]['us-east-1'] = []
		else:
			times = [float(t) for t in line.replace('[', '').replace(',', '').replace(']', '').split()]
			results[size]['eu-west-1'].append(times[0])
			results[size]['ap-southeast-1'].append(times[1])
			results[size]['sa-east-1'].append(times[2])
			results[size]['us-east-1'].append(times[3])
	
	means = {}
	for s in results:
		means[s] = {}
		for r in results[s]:
			means[s][r] = stats.mean(results[s][r])#, stats.standard_deviation(results[s][r]))

	return means

def draw(name):
	data = parse_result(name)
	t1 = []	# eu-west-1
	t2 = []	# ap-southeast-1
	t3 = []	# sa-east-1
	t4 = []	# us-east-1
	sizes = []
	for s in data:
		sizes.append(s)
		t1.append(data[s]['eu-west-1'])
		t2.append(data[s]['ap-southeast-1'])
		t3.append(data[s]['sa-east-1'])
		t4.append(data[s]['us-east-1'])

	plt.plot(sizes, t1, 'rs', label='eu-west-1 (destination)')
	plt.plot(sizes, t1, 'r-')
	plt.plot(sizes, t2, 'go', label='ap-southeast-1')
	plt.plot(sizes, t2, 'g-')
	plt.plot(sizes, t3, 'yv', label='sa-east-1')
	plt.plot(sizes, t3, 'y-')
	plt.plot(sizes, t4, 'b^', label='us-east-1')
	plt.plot(sizes, t4, 'b-')

	plt.xlabel('Data size (mb)')
	plt.ylabel('Execution time (s)')
	# plt.axis((1,5,5,55))

	plt.legend(loc='upper left')
	plt.show()

def draw_1(name):
	data = parse_result(name)
	t1 = []	# eu-west-1
	t2 = []	# ap-southeast-1
	t3 = []	# sa-east-1
	t4 = []	# us-east-1
	sizes = []
	for s in data:
		sizes.append(s)
		t1.append(data[s]['eu-west-1'])
		t2.append(data[s]['ap-southeast-1'])
		t3.append(data[s]['sa-east-1'])
		t4.append(data[s]['us-east-1'])

	if 'diff' not in name:
		us = 0.808137388145
		ap = 0.833543790959
		sa = 1.81631010514
	else:
		us = 0.828742649838
		ap = 0.784285502922
		sa = 1.72359347179

	_t1 = []	# eu-west-1
	_t2 = []	# ap-southeast-1
	_t3 = []	# sa-east-1
	_t4 = []	# us-east-1

	for i in range(len(t1)):
		_t1.append(t1[i] / t1[i])
		_t2.append(t2[i] / t1[i])
		_t3.append(t3[i] / t1[i])
		_t4.append(t4[i] / t1[i])

	plt.plot(sizes, _t1, 'rs', label='eu-west-1 (destination)')
	plt.plot(sizes, _t1, 'r-')

	plt.plot(sizes, _t2, 'go', label='ap-southeast-1')
	plt.plot(sizes, _t2, 'g-')
	plt.plot(sizes, [ap for i in sizes], 'g--', label='predicted ap-southeast-1')

	plt.plot(sizes, _t3, 'yv', label='sa-east-1')
	plt.plot(sizes, _t3, 'y-')
	plt.plot(sizes, [sa for i in sizes], 'y--', label='predicted sa-east-1')

	plt.plot(sizes, _t4, 'b^', label='us-east-1')
	plt.plot(sizes, _t4, 'b-')
	plt.plot(sizes, [us for i in sizes], 'b--', label='predicted us-east-1')

	# plt.setp(l1, linewidth=2.0)

	plt.xlabel('Data size (mb)')
	plt.ylabel('Execution time (s)')
	plt.axis((1,5,0.5,2))

	plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
       ncol=4, mode="expand", borderaxespad=0.)
	plt.show()
