from data import data

def find_compute_unit(bandwidths=data.parse_data('bw_mean'),
	sources={'us-west-1':5,'us-west-2':5,'ap-northeast-1':5,'ap-southeast-2':5},
	avail_units=['ap-southeast-1','sa-east-1','us-east-1'], 
	dest='eu-west-1', shrinking_ratio=0.1):
	init_time = 0.0
	for s in sources:
		b = bandwidths[s][dest]
		# print '{0} > {1}: {2}'.format(s, dest, b)
		init_time = init_time + sources[s] / b

	unit_times = {}
	avail_units.append(dest)
	for u in avail_units:
		unit_times[u] = 0.0
		for s in sources:
			b1 = bandwidths[s][u]
			b2 = bandwidths[u][dest]
			size = sources[s]
			# print '{0} > {1}: {2}'.format(s, u, b1)
			# print '{0} > {1}: {2}'.format(u, dest, b2)
			unit_times[u] = unit_times[u] + (size / b1) + (shrinking_ratio * size) / b2

	for u in unit_times:
		print u, unit_times[u]/init_time

	if True: return dest, init_time, unit_times
	selected_unit = None
	min_time = init_time
	for u in unit_times:
		if unit_times[u] < min_time:
			min_time = unit_times[u]
			selected_unit = u
	return dest, init_time, selected_unit, min_time, (init_time - min_time) / init_time