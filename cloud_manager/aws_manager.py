import boto
from boto import ec2

cherrypy_images = {
			 'us-east-1':'ami-bb1415d2',
			'us-west-2':'ami-d2c0ade2',
			'us-west-1':'ami-94eed2d1',
			'eu-west-1':'ami-2236c555',
			'ap-southeast-1':'ami-ee2a7bbc',
			'ap-northeast-1':'ami-d12c5bd0',
			'ap-southeast-2':'ami-71e1784b',
			'sa-east-1':'ami-f7bb18ea'
		}

class AWS_Manager:
	def __init__(self, verbose=False, images=cherrypy_images):
		self.key_name = 'C:\\fakepath\\longthaikey'
		self.instance_type = 't1.micro'
		self.security_groups = ['metric-collector']
		self.instances = {}
		self.images = images
		if verbose: boto.set_stream_logger('foo')

	def check_all_instances_status(self, desired_status='running'):
		"""
		Check if all instances have the same give status
		"""
		for instance in self.instances:
			status = self.instances[instance].update()
			if status != desired_status:
				return False	# return False if exist an instance whose status is different than the desired one

		return True

	def update_running_instances(self):
		self.instances = {}
		self.instances = self.get_instances()

	def get_instances(self, status='running', *regions):
		"""
		Get instances with the specified status at regions
		"""
		if not regions:
			regions = self.images.keys()

		instances = {}
		for r in regions:
			conn = self.get_connection_to_region(r)
			reservations = conn.get_all_instances()
			for r in reservations:
				for i in r.instances:
					stt = i.update()
					if stt == status: instances[i.id] = i # add instance if it is in given state
					else: print i, stt

		return instances

	def start_instances(self, region_to_num=None, ignore_running=False):
		"""
		Start instance(s) at multiple regions.
		region_to_num is the dict from region name to number of instances to start
		"""
		if region_to_num == None:
			region_to_num = {}
			for region in ec2.regions:
				if 'gov' in region.name or region.name not in self.images : continue
				region_to_num[region] = 1

		for region in region_to_num:
			self.start_instances_at_region(region, region_to_num[region], ignore_running)

	def start_instance(self, region_name, ignore_running=False):
		"""
		Start 1 instance at the given region.
		This method is kept in order to ensure backward compatibility
		"""
		self.start_instances(region_name, ignore_running)		
	
	def start_instances_at_region(self, region, num=1, ignore_running=False):
		"""
		Start num instances at the given region.
		If ignore_running is True, new instance(s) will be started regardless of the running ones
		"""

		if 'gov' in region or region not in self.images:
			raise Exception('Invalid region {0}'.format(region))

		conn = self.get_connection_to_region(region)
		num_running_instances = 0

		# find number of running instances
		if not ignore_running:
			num_running_instances = len(self.get_instances('running', region))

		num_instances_to_start = num
		# if (num > num_running_instances):
		# 	num_instances_to_start = num - num_running_instances	# calculate remaining number of instances to start
		# else:
		# 	num_instances_to_start = 0

		print 'There are {0} instances to start at region {1}'.format(num_instances_to_start, region)
		for count in range(num_instances_to_start):
			reservation = conn.run_instances(self.images[region], 
				key_name=self.key_name, security_groups=self.security_groups, instance_type=self.instance_type)
			for i in reservation.instances:
				print 'Starting instance {0} at region {1}'.format(i.id, region)
				self.instances[i.id] = i	

	def terminate_instances(self, *instance_ids):
		"""
		Terminate instances.
		If no instance id is given, terminate all instances
		"""
		if not instance_ids:
			instance_ids = self.instances.keys()

		for instance_id in instance_ids:
			self.terminate_instance(instance_id)

	def terminate_instance(self, instance_id):
		if instance_id not in self.instances: raise Exception('Unknown instance id {0}'.format(instance_id))

		instance = self.instances[instance_id]
		region = instance.region.name
		conn = ec2.connect_to_region(region)
		print 'Terminating instsances {0}'.format(instance)
		conn.terminate_instances([instance.id])				# terminate instance
		self.instances.pop(instance_id)						# remove instance from internal cache

	def get_regions(self):
		"""
		Get region -> instance id -> ip address
		"""
		regions = {}
		for instance in self.instances.values():
			# get and add region to map
			region = instance.region.name
			if region not in regions: regions[region] = []

			# connec to region and get ip address of running instance
			conn = ec2.connect_to_region(region)
			instance_queue = conn.get_only_instances([instance.id])
			if len(instance_queue) > 0:
				ip = instance_queue.pop().ip_address
				regions[region].append(ip)
		return regions

	def get_ips(self):
		"""
		Get ip address -> region
		"""
		ips = {}
		for instance in self.instances.values():
			# get and add region to map
			region = instance.region.name

			# connec to region and get ip address of running instance
			conn = ec2.connect_to_region(region)
			instance_queue = conn.get_only_instances([instance.id])
			if len(instance_queue) > 0:
				ip = instance_queue.pop().ip_address
				ips[ip] = region
		return ips

	def get_region(self, region_name):
		"""
		Get region object of a given region name
		"""
		regions = ec2.regions()
		for region in regions:
			if region.name == region_name :
				return region
		raise Exception('Unknown region {0}'.format(region_name))

	def get_connection_to_region(self, region_name):
		region = self.get_region(region_name)
		conn = ec2.connect_to_region(region.name)
		return conn