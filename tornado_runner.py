import tornado.ioloop
import tornado.web
import tornado.options
import logging
import sys
import importlib

class Producer(tornado.web.RequestHandler):
	def post(self):
		size = self.get_argument("data", default=None, strip=False)
		self.write(''.join(['0' for i in range(int(size))]))

if __name__ == "__main__":

	if len(sys.argv) == 1: sys.exit()

	applications = []
	for i in range(1, len(sys.argv)):
		arg = sys.argv[i]
		module_name = None
		class_name = None
		if '.' in arg: # if module name is included
			class_name = arg.split('.')[-1]
			module_name = arg.replace('.' + class_name, '')
		else: # assume that module name is similar to class name
			module_name = class_name = arg

		# get module and (service) class
		module = importlib.import_module(module_name)
		service_class = getattr(module, class_name)

		# add service's path and class
		applications.append(('/{0}/'.format(class_name), service_class))
		print 'Adding service {0}'.format(class_name)

	application = tornado.web.Application(applications)

	tornado.options.parse_command_line()
	logging.info("starting tornado web server")
	application.listen(8080)
	tornado.ioloop.IOLoop.instance().start()
