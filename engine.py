import time
from collections import OrderedDict
import logging
import ast
import threading
from threading import Thread

import requests

from service_invocation import Service_Invocation
from service_invocation import Service_Invocation_State as State

# init logger
logger=logging.getLogger('engine')
if not len(logger.handlers):
	formatter = logging.Formatter(fmt='%(asctime)s - %(levelname)s - %(module)s - %(message)s')
	handler = logging.FileHandler('./engine.log')
	handler.setFormatter(formatter)
	logger = logging.getLogger("engine")
	logger.setLevel(logging.DEBUG)
	logger.addHandler(handler)

class Engine:
	"""
	A data driven workflow engine: it invokes services when all required data is available.
	"""

	def __init__(self, engine_host, plan, port=8080, sequential=False):
		self.engine_host = engine_host
		self.port = port

		# flags to invoke services in sequence, i.e. prevent parallel invocation
		self.sequential = ast.literal_eval(sequential)

		self.is_executing = False

		self.data = {}
		self.invocations = []

		self.lock = threading.RLock()

		global logger
		self.logger = logger

		if type(plan) != list:
			plan = [plan]
		for line in plan:
			self.parse_script(line)

		self.isFinished = False
		self.start = None
		self.end = None

		self.isTerminated = False

		self.input_cache, self.output_cache = create_caches(self.invocations)

		logger.info('Initialise engine at {0}'.format(engine_host))
		logger.info('Engine {0} has invocations {1}'.format(engine_host, self.invocations))
		logger.info('Engine {0} is sequential {1}'.format(engine_host, self.sequential))
		logger.info('Input cache: {0}'.format(self.input_cache))
		logger.info('Output cache: {0}'.format(self.output_cache))

		self.isReady = False
		# produce required data if necessary
		for i in self.invocations:
			if i.service_name == 'Engine_Data_Producer':
				self.invoke_service(i, self.get_invocation_inputs(i.input_string))
		self.isReady = True

	def invoke_service(self, invocation, inputs):
		host = invocation.host
		command = "http://{0}:{1}/{2}".format(host, self.port, invocation.service_name)
		
		print "Invoking service {0} at {1}".format(invocation.service_name, host)
		self.logger.info("Invoking service {0} at {1}".format(invocation.service_name, host))
		self.logger.info(command)
		start = time.time()
		try:
			r = requests.post(command, inputs)

			end = time.time()
			if r.status_code != 200:
				self.logger.error(r.text)
				raise Exception(r.text)
			self.data[invocation.output] = r.text
			invocation.state = State.INVOKED
			print "Invoking {0} take {1} seconds".format(invocation, end - start)
			self.logger.info("Invoking {0} take {1} seconds".format(invocation, end - start))

			# clear data if possible
			self.clear_cache(invocation.output)

			self.invocations.remove(invocation)
		except ValueError:
			self.logger.error('Unknown invocation {0}'.format(invocation))
		# except Exception as e:
		# 	self.logger.error('Encounter exception {0}'.format(e))

			# wait (i.e. sleep for a while)
			time.sleep(10)

			# increase retry_count
			invocation.retry_count = invocation.retry_count + 1
			if (invocation.retry_count >= 5):
				# in retry threshold is reached, terminate workflow execution
				self.isTerminated = True
				self.logger.error('Terminate workflow execution due to error in {0}'.format(invocation))
			else:
				# set invocation state to WAITING so that it can be executed again
				invocation.state = State.WAITING


		# turn off this flag to allow another service to be invoked if possible
		self.is_executing = False
		# continue invoking other services if possible
		self.check_and_invoke_service()

	def check_and_invoke_service(self):
		t = None
		with self.lock:
			# don't continue of engine is not ready or has either finished or terminated
			if (not self.isReady or self.isFinished or self.isTerminated):
				self.logger.debug('{0} {1} {2}'.format(self.isReady, self.isFinished, self.isTerminated))
				return

			if (self.sequential and self.is_executing):
				self.logger.debug('Waiting for another service invocation to finish')
				return # don't invoke multiple service at the same time

			for i in self.invocations:
				if i.state == State.WAITING:
					inputs = self.get_invocation_inputs(i.input_string)
					if inputs != None: # data is ready
						i.state = State.INVOKING

						# record the start time if necessary
						if (self.start == None):
							self.start = time.time()
							print 'Start executing workflow'
							self.logger.info('Start executing workflow')

						self.is_executing = True
						# invoke service in the new thread so that it will not block the execution
						t = Thread(target=self.invoke_service, args=(i, inputs))
						t.start()
						# t.join()
						# self.invoke_service(i, inputs)

						# turn on this flag to prevent parallel invocation if required
						if (self.sequential and self.is_executing):
							break # don't continue
					else:
						print "Data for invocation {0} is not ready".format(i)
						self.logger.debug("Data for invocation {0} is not ready".format(i))

			isFinished = all(i.state == State.INVOKED for i in self.invocations)
			if (isFinished):
				self.end = time.time()
				self.isFinished = True

				print 'Workflow execution time: {0}'.format(self.end - self.start)
				self.logger.info('Workflow execution time: {0}'.format(self.end - self.start))


	# host.service_name input_name1:input1,input_name2:input2 output
	def parse_script(self, input_string):
		tokens = input_string.split(" ")
		if len(tokens) != 3: raise Exception("Invalid script line {0}".format(input_string))

		host, service_name = tokens[0].rsplit(".", 1)
		input_string = tokens[1].strip()
		output = tokens[2].strip()
		
		invocation = Service_Invocation(host, service_name, input_string, output)

		print "Adding service invocation {0}".format(invocation)
		self.logger.info("Adding service invocation {0}".format(invocation))

		self.invocations.append(invocation)

	def get_invocation_inputs(self, input_string):
		"""
		Check/Find all the inputs required to an invocation.
		If all inputs are available, return a map from input name to input data.
		Otherwise, return None
		"""

		'''
		'input' >> input
		input >> data[input]
		'''

		self.logger.debug('Looking for input string {0}'.format(input_string))
		input_pairs = input_string.split(",")
		inputs = {}
		try:
			for p in input_pairs:
				_input_name, input_value = p.split(":")
				input_name = self.get_data(_input_name)
				input_data = self.get_data(input_value)

				if input_name in inputs: # if input with the same name already exists, that mean input is a list
					if type(inputs[input_name]) != list:	# convert input to list if necessary
						tmp = inputs[input_name]
						inputs[input_name] = []
						inputs[input_name].append(tmp)
					inputs[input_name].append(input_data)
				else:										# input is just a normal value
					inputs[input_name] = input_data
			return inputs
		except KeyError as e:
			self.logger.debug('Missing data {0}'.format(e))
			return None

	def get_data(self, input_value):
		try:
			if '.' in input_value:
				num_input_value = float(input_value)
			else:
				num_input_value = int(input_value)
			return num_input_value
		except ValueError:
			pass

		if (input_value.startswith("'") and input_value.endswith("'")):
			# if input are wrap in single quotation mark ('), return the same value
			return input_value.replace("'", "")
		elif(isinstance(input_value, (int, float, long))):
			# if input is integer, float or double, return without modification
			return input_value
		else:
			# else, get data from cache
			return self.data[input_value]

	def clear_cache(self, output):
		with self.lock:
			try:
				# get all inputs of given output
				inputs = self.output_cache[output]

				for i in inputs:
					# get each input, get all outputs
					outputs = self.input_cache[i]
					if output in outputs:
						outputs.remove(output) # remove output from list of outputs requiring current input

						# remove input data if there is no output left
						if len(outputs) == 0:
							if i in self.data:
								self.logger.info('Remove input {0} from data cache'.format(i))
								del self.data[i]
								self.logger.debug('There are {0} data left in cache'.format(self.data.keys()))
						else:
							self.logger.debug('Input {0} is still required by {1} outputs'.format(i, outputs.keys()))
					else:
						self.logger.error('Output {0} does not require input {1}'.format(output, i))
			except KeyError as e:
				self.logger.error(str(e))

def create_caches(invocations):
	input_cache = {}
	output_cache = {}
	for i in invocations:
		output = i.output

		# add output to output cache
		if output not in output_cache:
			output_cache[output] = []

		input_strings = i.input_string.split(',')
		for input_string in input_strings:
			input_tokens = input_string.split(':')
			for token in input_tokens:
				if "'" not in token:
					try:
						float(token)
					except Exception:
						# add input to input cache
						if token not in input_cache:
							input_cache[token] = []
						# add outout to input cache
						if output not in input_cache[token]:
							input_cache[token].append(output)
						# add input to output cache
						if token not in output_cache[output]:
							output_cache[output].append(token)
	return input_cache, output_cache

engine = None

class Engine_Initialiser:
	exposed = True

	def POST(self, engine_host, plan, sequential):
		global engine
		engine = Engine(engine_host=engine_host, plan=plan, sequential=sequential)

		t = Thread(target=engine.check_and_invoke_service)
		t.start()
		# engine.check_and_invoke_service()

		return str('')

class Engine_Data_Getter:
	exposed = True

	def POST(self, data_id):
		global engine
		data = engine.data[data_id]
		engine.logger.info('Returning data for id {0}'.format(data_id))
		return str(data)

class Engine_Data_Setter:
	exposed = True

	def POST(self, **data):
		global engine
		for k in data:
			engine.logger.info('Setting data {0}'.format(k))
			engine.data[k] = data[k]
			engine.logger.info('Finish setting data {0}'.format(k))
		engine.check_and_invoke_service()
		return ''

class Engine_Data_Producer:
	exposed = True

	def POST(self, data):
		return ''.join(['0' for i in range(int(data))])