from metric_collector import stats

def parse_metrics(filename):
	if '/' in filename:
		aws = filename.split('/')[-1]
	else:
		aws = filename

	with open(filename) as f:
		lines = [l for l in f.readlines() if 'take' in l]

	pl_nodes = {}
	for l in lines:
		tokens = l.split()
		pl_node = tokens[8].replace('[', '').replace(',', '')
		if pl_node not in pl_nodes: pl_nodes[pl_node] = []
		time = float(tokens[-2])
		pl_nodes[pl_node].append(time)

	res = {}
	for n in pl_nodes:
		data = sorted(pl_nodes[n])[:10]
		mean = stats.mean(data)
		std = stats.standard_deviation(data)
		res[n] = (mean, std)

	return res

