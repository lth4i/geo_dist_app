# run a service. Example: python service_runner.py util.distance_calculator.Distance_Service

if __name__ == '__main__':
	import logging

	import sys
	import imp

	import cherrypy

	if len(sys.argv) == 1: sys.exit()

	for i in range(1, len(sys.argv)):
		arg = sys.argv[i]
		module_name = None
		class_name = None
		if '.' in arg: # if module name is included
			class_name = arg.split('.')[-1]
			module_name = arg.replace('.' + class_name, '')
		else: # assume that module name is similar to class name
			module_name = class_name = arg

		# get module and (service) class
		module = imp.load_source(module_name, module_name.replace('.', '/') + '.py')
		service_class = getattr(module, class_name)

		cherrypy.tree.mount(service_class(), '/' + class_name,{'/':
				{'request.dispatch': cherrypy.dispatch.MethodDispatcher()}
			})
		print 'Adding service', class_name

	cherrypy.config.update({'server.socket_host': '0.0.0.0',
			'server.socket_port': 8080,
		})

	# increase the request body size to 1GB
	cherrypy.server.max_request_body_size = 1048576000
	cherrypy.config.update({'log.screen': True})
	cherrypy.config.update({'log.access_file': 'server.log'})
	cherrypy.config.update({'log.error_file': 'server.log'})


	cherrypy.engine.start()
	cherrypy.engine.block()