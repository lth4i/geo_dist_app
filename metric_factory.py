import time

from cloud_manager.aws_manager import AWS_Manager

import requests

class Location:
	def __init__(self, ip, name):
		self.ip = ip
		self.name = name

	def __str__(self):
		return self.__class__.__name__ + ':' + self.ip + ', ' + self.name

	def __repr__(self):
		return self.__str__()

class CloudRegion(Location):
	def __init__(self, ip, name):
		Location.__init__(self, ip, name)

	def get_distance_service(self):
		return 'http://' + self.ip + '/Distance_Service?wsdl'

	def get_metric_collector_service(self):
		return 'http://' + self.ip + '/Metric_Collector/'

class Metric_Factory:
	def __init__(self, am=AWS_Manager(), regions=[], services=[]):
		self.am = am
		self.regions = regions
		self.services = services

	def wait_and_check_status(self, desired_status, num_of_sleep=10, sleep_time=10):
		for count in range(num_of_sleep):
			if self.am.check_all_instances_status(desired_status):
				print 'All instances are at desired status:', desired_status
				return True
			else:
				time.sleep(sleep_time)
		return False

	def start_collecting_metrics(self):
		# make sure that all instances are running
		if not self.wait_and_check_status('running'):
			raise Exception('Not all instances are running')

		self.regions = []
		ips = self.am.get_ips()
		for ip in ips:
			self.regions.append(CloudRegion(ip, ips[ip]))

		distances_region_region = {}
		distances_region_service = {}

		# calculate distances between Cloud regions
		for source in self.regions:

			distances_region_region[source.name] = {}
			distances_region_service[source.name] = {}

			# calculate distance between Cloud regions
			for destination in self.regions:
				print source.name, '->', destination.name
				distance = requests.get(source.get_metric_collector_service() + destination.ip)
				distances_region_region[source.name][destination.name] = distance.text

			# calculate distance between Cloud regions and services
			for destination in self.services:
				print source.name, '->', destination.name
				distance = requests.get(source.get_metric_collector_service() + destination.ip)
				distances_region_service[source.name][destination.name] = distance.text

		return distances_region_region, distances_region_service

def print_distances(distances):
	output = ''
	for source in distances:
		for destination in distances[source]:
			output = output + source + ' -> ' + destination + ': ' + str(distances[source][destination]) + ',\n'

	if len(output) > 2: output = output[:-2]
	return output


def get_services(file_name='services.dat'):
	data = {}
	with open(file_name) as f:
		lines = f.readlines()
		for line in lines:
			tokens = line.split('\t')
			data[tokens[0].strip()] = tokens[1].strip()

	services = []
	for host in data:
		services.append(Location(host, data[host]))

	return services