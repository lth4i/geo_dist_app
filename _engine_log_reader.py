# 2014-02-23 19:33:56,251 - INFO - engine - Start workflow
# 2014-02-23 19:34:02,549 - INFO - engine - Service invocation time: 5.31023693085
# 2014-02-23 19:31:30,587 - INFO - engine - Workflow execution time: 30.7195658684

import os
import glob

from metric_collector import stats

START_MARKER = 'Start workflow'
END_MARKER = 'Workflow execution time'
SERVICE_CALL_MARKER = 'Service invocation time'

def get_files(directory='./log/linear'):
	files = os.listdir(directory)
	for f in files:
		print f, parse_log('{0}/{1}'.format(directory, f))

def parse_logs(logs_dir='log/2014.03.12'):
	if not logs_dir.strip().endswith('/'): logs_dir = logs_dir.strip() + '/'
	logs = glob.glob(logs_dir.strip() + '*')

	results = {}
	for l in logs:
		results[l] = parse_log(l)

	return results

def parse_log(log_file):
	with open(log_file) as f:
		lines = f.readlines()

	exec_times = []
	for line in lines:
		if START_MARKER in line:
			continue # ignore this for now
		elif END_MARKER in line:
			tokens = line.split()
			exec_time = float(tokens[-1])
			exec_times.append(exec_time)
		elif SERVICE_CALL_MARKER in line:
			continue # ignore this for now
		else:
			continue

	return len(exec_times), stats.mean(exec_times), stats.standard_deviation(exec_times), max(exec_times), min(exec_times)

def get_service_invocation_time(log_file):
	with open(log_file) as f:
		lines = f.readlines()

	calling_times = {}
	for i in range(len(lines)):
		line = lines[i]
		if START_MARKER in line: continue

		if line.count('>') != 2: continue

		if i >= len(lines): continue

		next_line = lines[i + 1]
		if SERVICE_CALL_MARKER not in next_line: continue

		called_service = [t.strip() for t in line.split('>')][1].split('.')[0]
		calling_time = float(next_line.split()[-1])

		if called_service not in calling_times: calling_times[called_service] = []
		calling_times[called_service].append(calling_time)

	summary = {}
	for called_service in calling_times:
		summary[called_service] = stats.mean(calling_times[called_service])

	return summary

def draw():
	import matplotlib.pyplot as plt
	import numpy as np

	means_single = [36.48629503251, 33.21772871017001]
	std_single = [2.388012578495238, 3.531664884088235]
	means_multi = [28.018896555890002, 17.08680706025]
	std_multi = [2.6629824616487263, 1.519612201055462]

	fig, ax = plt.subplots()

	index = np.arange(2)
	bar_width = 0.25

	opacity = 0.4
	error_config = {'ecolor': '0.3'}

	rects1 = plt.bar(index, means_single, bar_width,
                 alpha=opacity,
                 color='b',
                 yerr=std_single,
                 error_kw=error_config,
                 label='Single Engine')

	rects2 = plt.bar(index + bar_width, means_multi, bar_width,
                 alpha=opacity,
                 color='r',
                 yerr=std_multi,
                 error_kw=error_config,
                 label='Multiple Engines')

	# plt.xlabel('Workflow Type')
	plt.ylabel('Execution Time (s)')
	plt.xlim([-.5, 3])
	# plt.title('Execution Times Comparison')
	plt.xticks(index + bar_width, ('Linear', 'Fan'))
	plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
       ncol=2, mode="expand", borderaxespad=0.)
	# plt.legend()

	# plt.tight_layout()
	plt.show()

def _draw():
	data = parse_logs()
	linears = filter(lambda x: 'linear' in x, data)
	fans = filter(lambda x: 'fan' in x, data)

	mins = []
	maxes = []
	middles = {}

	if len(linears) > len(fans):
		tmp = linears
	else:
		tmp = fans

	pass # TODO: too lazy to do...

def _draw_linear():
	import matplotlib.pyplot as plt
	import numpy as np


	means_single = [30.480513433625003]
	std_single = [7.194789969516671]
	means_mid = [23.326668284154547]
	std_mid = [0.8047552479498088]
	means_multi = [20.3596988808]
	std_multi = [0.9155837525653175]

	fig, ax = plt.subplots()

	index = np.arange(1)
	bar_width = 0.25

	opacity = 0.4
	error_config = {'ecolor': '0.3'}

	rects1 = plt.bar(index - bar_width, means_single, bar_width,
                 alpha=opacity,
                 color='b',
                 yerr=std_single,
                 error_kw=error_config,
                 label='Cost 1000, 1 Engine')

	rects2 = plt.bar(index, means_mid, bar_width,
                 alpha=opacity,
                 color='g',
                 yerr=std_mid,
                 error_kw=error_config,
                 label='Cost 500, 3 Engines')

	rects3 = plt.bar(index + bar_width, means_multi, bar_width,
                 alpha=opacity,
                 color='r',
                 yerr=std_multi,
                 error_kw=error_config,
                 label='Cost 1, 4 Engines')

	# plt.xlabel('Workflow Type')
	plt.ylabel('Execution Time (s)')
	plt.xlim([-2, 3])
	# plt.title('Execution Times Comparison')
	plt.xticks(index + bar_width, ('Linear', 'Fan'))
	plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
       ncol=3, mode="expand", borderaxespad=0.)
	# plt.legend()

	# plt.tight_layout()
	plt.show()

def _draw_fan():
	import matplotlib.pyplot as plt
	import numpy as np

	means_single = [23.465021872509997]
	std_single = [1.9588939358992505]
	means_1000 = [21.032175562618182]
	std_1000 = [1.3847255570583779]
	means_500 = [22.54467066850909]
	std_500 = [1.1426410644816059]
	means_100 = [12.726476842709092]
	std_100 = [0.4012538609399863]
	means_multi = [9.876275587082]
	std_multi = [0.17636376445334848,]

	fig, ax = plt.subplots()

	index = np.arange(1)
	bar_width = 0.25

	opacity = 0.4
	error_config = {'ecolor': '0.3'}

	rects0 = plt.bar(index - 2*bar_width, means_single, bar_width,
                 alpha=opacity,
                 color='c',
                 yerr=std_single,
                 error_kw=error_config,
                 label='Cost 1500, 1 Engine')

	rects1 = plt.bar(index - bar_width, means_1000, bar_width,
                 alpha=opacity,
                 color='b',
                 yerr=std_1000,
                 error_kw=error_config,
                 label='Cost 1000, 2 Engine')

	rects2 = plt.bar(index, means_500, bar_width,
                 alpha=opacity,
                 color='g',
                 yerr=std_500,
                 error_kw=error_config,
                 label='Cost 500, 3 Engines')

	rects3 = plt.bar(index + bar_width, means_100, bar_width,
                 alpha=opacity,
                 color='r',
                 yerr=std_100,
                 error_kw=error_config,
                 label='Cost 100, 4 Engines')

	rects4 = plt.bar(index + bar_width*2, means_multi, bar_width,
                 alpha=opacity,
                 color='y',
                 yerr=std_multi,
                 error_kw=error_config,
                 label='Cost 1, 5 Engines')

	# plt.xlabel('Workflow Type')
	plt.ylabel('Execution Time (s)')
	plt.xlim([-2, 3])
	# plt.title('Execution Times Comparison')
	plt.xticks(index + bar_width, ('Fan', 'Fan'))
	plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
       ncol=3, mode="expand", borderaxespad=0.)
	# plt.legend()

	# plt.tight_layout()
	plt.show()