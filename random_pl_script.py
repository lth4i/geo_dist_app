import planet_lab as pl
import subprocess

import requests

def produce_dummy_data():
	nodes = pl.get_nodes()
	for n in nodes:
		print n
		print 'http://{0}:8080/Producer'.format(n)
		r = requests.post('http://{0}:8080/Producer'.format(n), {'data':'1048576'})
		r = requests.post('http://{0}:8080/Producer'.format(n), {'data':'2097152'})
		r = requests.post('http://{0}:8080/Producer'.format(n), {'data':'5242880'})
		r = requests.post('http://{0}:8080/Producer'.format(n), {'data':'10485760'})


