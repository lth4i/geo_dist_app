import logging

formatter = logging.Formatter(fmt='%(asctime)s - %(levelname)s - %(module)s - %(message)s')
handler = logging.FileHandler('./consumer.log')
handler.setFormatter(formatter)
logger = logging.getLogger("engine")
logger.setLevel(logging.DEBUG)
logger.addHandler(handler)

def consume(data, r=1):
	if r < 0:
		raise ValueError('Ratio must be larger than 0: {0}'.format(r))
	r = float(r)

	old_size = len(data)
	logger.info('Receive data with size {0}'.format(old_size))
	if r == 1:
		processed_data = data
	elif r < 1:
		new_size = int(r * old_size) # change, i.e. reduce, data size
		processed_data = data[:new_size]
	else:
		_r = int(r)
		processed_data = ''.join(data for i in range(_r))
		if r > _r:
			remaining_data = data[:int((r - _r) * old_size)]
			processed_data = ''.join([processed_data, remaining_data])

	logger.info('Return data with size {0}'.format(len(processed_data)))
	return processed_data

class _Consumer:
	exposed = True

	def POST(self, data):
		raw_data = data[0]
		r = data[1]
		return str(consume(raw_data, r))

class Consumer:
	exposed = True

	def POST(self, **data):
		raw_data = data['raw_data']
		r = data['ratio']
		return str(consume(raw_data, r))