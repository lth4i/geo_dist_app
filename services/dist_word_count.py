import requests
import sys
import time
import logging
from threading import Thread

# init logger
logger=logging.getLogger('services')
if not len(logger.handlers):
	formatter = logging.Formatter(fmt='%(asctime)s - %(levelname)s - %(module)s - %(message)s')
	handler = logging.FileHandler('./services.log')
	handler.setFormatter(formatter)
	logger = logging.getLogger("services")
	logger.setLevel(logging.DEBUG)
	logger.addHandler(handler)

def get_data(url, file_name):
	start = time.time()
	r = requests.post("http://{0}:8080/GetLines".format(url), {'data':file_name})
	end = time.time()
	logger.info("It takes [" + str(end - start) + "] seconds to get [" + file_name + "] from [" + url + "]")
	print "It takes [" + str(end - start) + "] seconds to get [" + file_name + "] from [" + url + "]"
	return r

def do_count(url, file_name):
	start = time.time()
	lines = get_data(url, file_name)
	end = time.time()
	total_time = end - start
	file_size = float(sys.getsizeof(''.join([l for l in lines]))) / (1024**2)
	speed = float(total_time) / float(file_size)
	logger.info("Finish getting [" + file_name + "] with size [" + str(file_size) + "] from [" + url +  "] in [" + str(total_time) + "] seconds with speed [" + str(speed) + "] s/mb")
	print "Finish getting [" + file_name + "] with size [" + str(file_size) + "] from [" + url +  "] in [" + str(total_time) + "] seconds with speed [" + str(speed) + "] s/mb"

	counts = {}
	start = time.time()
	for l in lines:
		words = [w.strip() for w in l.split() if w.strip() != '']
		for w in words:
			if w not in counts: counts[w] = 0
			counts[w] = counts[w] + 1
	end = time.time()
	total_time = end - start
	speed = float(total_time) / float(file_size)
	logger.info("Finish counting [" + file_name + "] with size [" + str(file_size) + "] from [" + url +  "] in [" + str(total_time) + "] seconds with speed [" + str(speed) + "] s/mb")
	print "Finish counting [" + file_name + "] with size [" + str(file_size) + "] from [" + url +  "] in [" + str(total_time) + "] seconds with speed [" + str(speed) + "] s/mb"

def do_counts(data):
	pass
	start = time.time()
	for url in data:
		file_names = data[url]
		if type(file_names) != list:
			file_names = [file_names]
		for file_name in file_names:
			_start = time.time()
			do_count(url, file_name)
			_end = time.time()
			total_time = str(_end - _start)
			logger.info("Finish counting [" + file_name + "] from [" + url +  "] in [" + str(total_time) + "] seconds")
			print "Finish counting [" + file_name + "] from [" + url +  "] in [" + str(total_time) + "] seconds"

	end = time.time()
	logger.info("Finish in [" + str(end - start) + "] seconds")
	print "Finish in [" + str(end - start) + "] seconds"

class WordCount:
	exposed = True

	def POST(self, **data):
		t = Thread(target=do_counts, args=(data,))
		t.start()
		return str('')