import random
import requests
import time

def produce(size=131072, upper_bound=9999999):
	return ''.join(['{0} '.format(`random.randint(0, upper_bound)`).rjust(8) for i in range(size)])

class Num_Producer:
	exposed = True

	def GET(self, size):
		return produce(int(size))

class Network_Test:
	exposed = True

	def GET(self, host, size):
		start = time.time()
		r = requests.get('http://{0}:8080/Num_Producer/{1}'.format(host, size))
		if r.status_code != 200: raise Exception(r.text)
		end = time.time()
		print len(r.text)
		return str(end - start)

class Network_Test_1:
	exposed = True

	def GET(self, host, size):
		start = time.time()
		r = requests.post('http://{0}:8080/Producer/'.format(host), {'data':size})
		if r.status_code != 200: raise Exception(r.text)
		end = time.time()
		print len(r.text)
		return str(end - start)
