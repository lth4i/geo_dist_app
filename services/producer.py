import random

def produce_stupid_log(data):
	size = int(data) / 8
	return ''.join(['{0}\n'.format('ERROR'.ljust(7,'0')) if random.randint(0, 9) == 0 else '{0}\n'.format('INFO'.ljust(7,'0')) for i in range(size)])

class Producer:
	exposed = True

	def POST(self, data):
		return ''.join(['0' for i in range(int(data))])


class Log_Producer:
	exposed = True

	def POST(self, data):
		return produce_stupid_log(data)

	def GET(self, data):
		return produce_stupid_log(data)
		
