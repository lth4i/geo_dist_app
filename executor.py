import time
import requests

from threading import Thread

import subprocess
import re

import interpreter
from cloud_manager.aws_manager import AWS_Manager
import deployment
# from engine import Engine

class Executor:
	def __init__(self, script, sequential=False):
		self.services, self.hosts, self.deployment_plan, self.engines = \
			interpreter.interpret_script_file(script)

		self.aws_manager = AWS_Manager()

		self.sequential = sequential

		self.hosts_by_region = {}
		for h in  self.hosts:
			host = self.hosts[h]
			region = host.region
			if region not in self.hosts_by_region:
				self.hosts_by_region[region] = []
			self.hosts_by_region[region].append(host)
		# self.engine = Engine(hosts=self.get_simple_hosts(), plan=self.execution_plan)

	def start_host(self, ignore_running=False):
		self.aws_manager.update_running_instances()

		aws_regions = self.aws_manager.get_regions()
		num_hosts_to_start = {}

		# check number of instances needed to be start at each AWS region
		for r in self.hosts_by_region:
			if r not in self.aws_manager.images: # for region which is unknown by AWS
				for h in self.hosts_by_region[r]:
					if h.host == '_':
						raise Exception('Host {0} is not known by AWS and is not running'.format(h))
			else: # for region which is known by AWS
				num_hosts = len(self.hosts_by_region[r])
				num_aws_hosts = len(aws_regions[r]) if r in aws_regions else 0
				if num_hosts > num_aws_hosts:	# if there is more host in script than in AWS region, start some more
					num_hosts_to_start[r] = num_hosts - num_aws_hosts
				else:							# otherwise, don't start any
					num_hosts_to_start[r] = 0
				print 'There are {0} instance(s) needed to be started at {1}'.format(num_hosts_to_start[r], r)

		# start instance(s) at each AWS region
		for r in num_hosts_to_start:
			if num_hosts_to_start[r] > 0:
				print 'Starting {0} instances at {1}'.format(num_hosts_to_start[r], r)
				self.aws_manager.start_instances_at_region(region=r, num=num_hosts_to_start[r], ignore_running=ignore_running)

		# wait and check status of AWS instances
		for i in range(10):
			if self.aws_manager.check_all_instances_status():
				self.update_aws_hosts(num_hosts_to_start.keys())
				return
			else:
				time.sleep(10)
		raise Exception('Not all instances are running')
		
	def update_aws_hosts(self, regions_to_check):
		# assign aws instance ips to hosts
		aws_regions = self.aws_manager.get_regions()
		for r in regions_to_check:
			hosts = self.hosts_by_region[r]
			instances = aws_regions[r]
			print hosts
			print instances
			for c in range(len(hosts)):
				h = hosts[c]
				i = instances[c]
				h.host = i
		self.update_engines()

	def stop_hosts(self):
		# first, remove ip/host from all aws instances
		self.aws_manager.check_all_instances_status()
		regions = self.aws_manager.get_regions()
		for r in regions:
			hosts = self.hosts_by_region[r]
			for h in hosts:
				h.host = '_'

		# stop aws instances
		self.aws_manager.terminate_instances()

	def deploy_services(self, ignore_running=False):
		ts = []
		for host_alias in self.deployment_plan:
			host_dom = self.hosts[host_alias]

			service_doms = []
			service_names = self.deployment_plan[host_alias]
			for service_name in service_names:
				service_dom = self.services[service_name]
				service_doms.append(service_dom)

			print host_dom.username, host_dom.host, service_doms
			t = Thread(target=deployment.do_deployment, args=(host_dom.username, host_dom.host, service_doms, ignore_running))
			ts.append(t)
			# deployment.do_deployment(host_dom.username, host_dom.host, service_doms, ignore_running)
			t.start()

		in_progress = len(ts) > 0
		while (in_progress):
			all_done = True
			for t in ts:
				# t.join()
				if t.isAlive():
					all_done = False
					break
			if all_done: in_progress = False
			else: time.sleep(1)

	def terminate_services(self):
		ts = []
		for host_alias in self.deployment_plan:
			host_dom = self.hosts[host_alias]
			# deployment.terminate_deployment(username=host_dom.username, host=host_dom.host)
			t = Thread(target=deployment.terminate_deployment, args=(host_dom.username, host_dom.host))
			ts.append(t)
			t.start()

		in_progress = len(ts) > 0
		while (in_progress):
			all_done = True
			for t in ts:
				# t.join()
				if t.isAlive():
					all_done = False
					break
			if all_done: in_progress = False
			else: time.sleep(1)

	def get_simple_hosts(self):
		simple_hosts = {}
		for host in self.hosts:
			simple_hosts[host] = self.hosts[host].host
		return simple_hosts

	def update_engines(self):
		for e in self.engines:
			lines = self.engines[e]
			new_lines = []
			for l in lines:
				host_alias = l.split()[0].split('.')[0]
				if host_alias in self.hosts:
					print 'Replacing {0} with {1}'.format(host_alias, self.hosts[host_alias].host)
					l = l.replace(host_alias, self.hosts[host_alias].host, 1)
				
				new_lines.append(l)
			self.engines[e] = new_lines

	def init_engines(self):
		for engine in self.engines:

			host = self.hosts[engine].host
			r = requests.post('http://{0}:8080/Engine_Initialiser/'.format(host),
				{'engine_host':host, 'plan':self.engines[engine], 'sequential':self.sequential})
			if r.status_code != 200:	# raise exception if status code is not 200
				raise Exception(r.text)

	def get_engine_log_commands(self):
		for e in self.engines:
			host = self.hosts[e]
			print 'scp {0}@{1}:~/engine.log {2}.log'.format(host.username, host.host, e)

	def get_engine_ssh_commands(self):
		comms = {}
		for e in self.engines:
			host = self.hosts[e]
			print e, 'ssh -i .ssh/longthaikey {0}@{1}'.format(host.username, host.host)

	def monitor_engine_log(self, engine, log_file_name='engine.log', matching_str='Workflow execution time', count=1,
		previous_str=''):
		if engine not in self.engines: raise Exception('Unknown engine {0}'.format(engine))
		engine_host = self.hosts[engine]
		for c in range(count):
			remote_tail = subprocess.Popen(['ssh', '{0}@{1}'.format(engine_host.username, engine_host.host),
				'tail', '-f', log_file_name], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			while 1:
				line = remote_tail.stdout.readline()
				# print line
				if not line or matching_str not in line or line == previous_str:
					continue
				else:
					# print 'Found {0}'.format(line)
					remote_tail.stdout.close()
					remote_tail.kill()
					break
		# 	print 'Breaking out of inner loop'
		# print 'Breaking out of outer loop'

		return line