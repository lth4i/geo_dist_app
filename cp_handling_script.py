import re
from glob import glob

def build_execution_plan(plan, ser_to_eng):
	if type(plan) != list:
		with open(plan) as f:
			lines = [l.strip() for l in f.readlines() if l.strip() != '']
		plan = lines

	aliases = {}
	aliases['CR_ap_northeast_1'] = 'an1'
	aliases['CR_ap_southeast_1'] = 'as1'
	aliases['CR_ap_southeast_2'] = 'as2'
	aliases['CR_eu_west_1'] = 'ew1'
	aliases['CR_sa_east_1'] = 'se1'
	aliases['CR_us_east_1'] = 'ue1'
	aliases['CR_us_west_1'] = 'uw1'
	aliases['CR_us_west_2'] = 'uw2'

	print '# Engines'
	if type(ser_to_eng) != dict:
		eng_to_ser = get_engine_function(ser_to_eng)
		_ser_to_eng = {}
		_existing_hosts = []
		for e in eng_to_ser:
			for s in eng_to_ser[e]:
				_e = aliases[e.strip()]
				if _e not in _existing_hosts:
					_existing_hosts.append(_e)
					print 'host {0} {1} ubuntu _'.format(_e.strip(), e.strip().replace('_', '-').replace('CR-', ''))
				_ser_to_eng[s] = _e
		ser_to_eng = _ser_to_eng

	print ''
	print '# Engines deployment'
	for _e in _existing_hosts:
		print 'depl {0} Engine_Initialiser,Engine_Data_Getter,Engine_Data_Producer,Engine_Data_Setter'.format(_e)

	print ''
	print '# Service invocations'
	services = {}
	line_to_service = {}
	produce = {}
	for i in plan:
		s = i.split('.')[0]
		# if s not in services: services[s] = []
		# services[s].append(i)
		services[s] = i
		line_to_service[i] = s

		v = i.split()[-1]
		if v not in produce: produce[v] = i
		else: raise Exception('{0} is produced by {1} and {2}'.format(v, produce[v], i))
	
	use = {}
	for v in produce:
		for i in plan:
			if re.search('\\{0}\\W'.format(v), i) and not i.endswith(v):
				if i not in use: use[i] = []
				use[i].append(v)

	steps = []
	count = 1
	for l in plan:
		s = line_to_service[l]
		e = ser_to_eng[s]

		if l in use:
			inputs = use[l]
			for i in inputs:
				# print l, i
				_l = produce[i]
				_s = line_to_service[_l]
				_e = ser_to_eng[_s]
				if e != _e:
					s = "{0} {1}.Engine_Data_Setter '{2}':{2} $_{3}".format(_e, e, i, count)
					if s not in steps:
						steps.append(s)
						count = count + 1

		steps.append('{0} {1}'.format(e, l))

	for s in steps: print s

def compare_solutions(sol_dir, wf):
	if sol_dir.endswith('*'): pass
	elif sol_dir.endswith('/'): sol_dir = sol_dir + '*'
	else: sol_dir = sol_dir + '/*'
	files = glob(sol_dir)
	bw = []
	rtt = []
	for f in files:
		if wf in f:
			if 'bandwidth' in f: bw.append(f)
			elif 'rtt' in f: rtt.append(f)
			else: print 'Unknown file {0}'.format(f)

	min_num = 0
	tmp = {} # map from number of engines to files
	for f in bw + rtt:
		engs = get_engine_function(f)
		num = len(engs)
		if num not in tmp: tmp[num] = []
		# for t in tmp[num]:
		# 	if 'bandwidth' in t and 'bandwidth' in f: continue
		# 	elif 'rtt' in t and 'rtt' in f: continue
		tmp[num].append(f)

	for t in tmp:
		fs = tmp[t]
		bw_f = None
		rtt_f = None
		for f in fs:
			if bw_f and rtt_f: break
			elif bw_f == None and 'bandwidth' in f: bw_f = f
			elif rtt_f == None and 'rtt' in f: rtt_f = f
		if bw_f == None or rtt_f == None: print t, '\t', False, '\t', bw_f, rtt_f
		else:
			bw_eng = get_engine_function(bw_f)
			rtt_eng = get_engine_function(rtt_f)
			print t, '\t', bw_eng != rtt_eng, '\t', bw_f, rtt_f

def get_engine_function(file_name):
	with open(file_name) as f:
		lines = f.readlines()

	line = ''
	in_func = False
	for l in lines:
		if 'letting engine be' in l:
			in_func = True
		elif in_func == True:
			line = line + l.strip()
			if ')' in l: break
	line = line.replace('function(', '').replace(')', '')

	tokens = [t.strip() for t in line.split(',')]
	eng_func = {}
	for t in tokens:
		s, e = t.split(' -->')
		if e not in eng_func: eng_func[e] = []
		eng_func[e].append(s)

	return eng_func