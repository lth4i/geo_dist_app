from dateutil import parser
from metric_collector import stats
import matplotlib.pyplot as plt
import numpy as np
from glob import glob
from time import strptime
import datetime

rd = {'1': [28, 29, 29, 30, 30, 30, 31, 31, 32, 33],
 '100': [26, 26, 26, 27, 28, 28, 29, 29, 30, 31],
 '1000': [28, 28, 29, 29, 30, 30, 31, 31, 32, 33],
 '1500': [32.6092429161,
  33.6268789768,
  37.8698120117,
  38.8126981258,
  43.7505168915,
  43.8157660961,
  47.4441559315,
  48.2536978722,
  49.9547522068,
  50.1637899876],
 '500': [27, 27, 28, 28, 28, 29, 30, 30, 31, 32]}

def parse_log_file(file_path):
	with open(file_path) as f:
		lines = f.readlines()

	for l in lines:
		if 'Start' in l:
			return parse_multiple_engines(file_path)

	return parse_single_engine(file_path)

def create_log_report(file_path):
	data = parse_log_file(file_path)
	mean = stats.mean(data)
	std = stats.standard_deviation(data)
	print '{0}_nearest\t{1}\t{2}\t{3}\t{4}'.format(file_path, 1, data, mean, std)

def parse_multiple_engines(file_path):
	with open(file_path) as f:
		lines = f.readlines()

	starts = []
	ends = []
	times = []
	for line in lines:
		if line.strip() == '':
			continue
		elif 'Start' in line:
			starts.append(line)
		elif 'Workflow execution time' in line:
			ends.append(line)

	for i in range(len(starts)):
		# s = parser.parse(starts[i].split()[1])
		# e = parser.parse(ends[i].split()[1])
		s = datetime.datetime.strptime(starts[i].split()[1], '%H:%M:%S,%f')
		e = datetime.datetime.strptime(ends[i].split()[1], '%H:%M:%S,%f')
		times.append((e - s).total_seconds())

	times.sort()
	return times[:10]

def parse_single_engine(file_path):
	with open(file_path) as f:
		lines = f.readlines()

	times = []
	for line in lines:
		time = float(line.split()[-1])
		times.append(time)

	times.sort()
	return times[:10]

def parse_log_directory(log_dir):
	files = glob(log_dir)

	groups = {}
	for f in files:
		if '_' not in f.split('/')[-1]: continue
		prefix = f.split('/')[-1].split('_')[0]
		if prefix not in groups: groups[prefix] = []
		groups[prefix].append(f)

	results = {}
	for g in groups:
		results[g] = {} 
		for f in groups[g]:
			print f
			suffix = int(f.split('/')[-1].split('_')[1])
			# print f, stats.mean(parse_log_file(f))
			data = parse_log_file(f)
			results[g][suffix] = (stats.mean(data),
				stats.standard_deviation(data))

	for g in results:
		s = sorted(results[g].keys(), reverse=True)
		for i in s:
			print '{0}\t{1}\t{2}'.format(g, i, results[g][i])

	return results

def draw(log_dir_1, prefix):
	res1 = parse_log_directory(log_dir_1)[prefix]

	print res1


	keys = sorted(res1.keys(), reverse=True)
	if 500 not in keys:
		res1[500] = res1[keys[0]]
		keys = sorted(res1.keys(), reverse=True)

	# plt.plot(keys, [res1[k][0] for k in keys], linestyle="dashed", marker="o", color="green")
	# plt.plot(keys, [res2[k][0] for k in keys], linestyle="dashed", marker="o", color="blue")
	plt.errorbar(keys, [res1[k][0] for k in keys], yerr=[res1[k][1] for k in keys],
		marker="o", color="green", linewidth=3)
	# plt.errorbar(keys, [res2[k][0] for k in keys], yerr=[res2[k][1] for k in keys],
	# 	marker="o", color="blue")
	# plt.errorbar(keys, y, yerr=y_error, linestyle="None", marker="None", color="green")

	plt.legend(['1MB'], loc='upper left')

	plt.xlabel('Redeploy Cost')
	plt.ylabel('Execution Time (s)')
	plt.ylim(0, 50)
	plt.xlim(keys[-1] - 10, keys[0] + 10)
	plt.show()

def compare_and_draw(log_dir_1, log_dir_2, prefix):
	res1 = parse_log_directory(log_dir_1)[prefix]
	res2 = parse_log_directory(log_dir_2)[prefix]

	print res1
	print res2

	if len(res1) != len(res2):
		raise Exception('Different sizes')

	keys = sorted(res1.keys(), reverse=True)
	if 500 not in keys:
		res1[500] = res1[keys[0]]
		res2[500] = res2[keys[0]]
		keys = sorted(res1.keys(), reverse=True)

	# plt.plot(keys, [res1[k][0] for k in keys], linestyle="dashed", marker="o", color="green")
	# plt.plot(keys, [res2[k][0] for k in keys], linestyle="dashed", marker="o", color="blue")
	plt.errorbar(keys, [res1[k][0] for k in keys], yerr=[res1[k][1] for k in keys],
		marker="o", color="green", linewidth=3)
	plt.errorbar(keys, [res2[k][0] for k in keys], yerr=[res2[k][1] for k in keys],
		marker="o", color="blue", linewidth=3)
	# plt.errorbar(keys, y, yerr=y_error, linestyle="None", marker="None", color="green")

	plt.legend(['1MB', '2MB'], loc='upper left')

	plt.xlabel('Redeploy Cost')
	plt.ylabel('Execution Time (s)')
	plt.ylim(0, 50)
	plt.xlim(-5, 505)
	plt.show()

def compare_and_draw_2(log_dir_1, log_dir_2, log_dir_3, prefix):
	res1 = parse_log_directory(log_dir_1)[prefix]
	res2 = parse_log_directory(log_dir_2)[prefix]
	res3 = parse_log_directory(log_dir_3)[prefix]

	print res1
	print res2
	print res3

	if len(res1) != len(res2):
		raise Exception('Different sizes')

	keys = sorted(res1.keys(), reverse=True)

	# plt.plot(keys, [res1[k][0] for k in keys], linestyle="dashed", marker="o", color="green")
	# plt.plot(keys, [res2[k][0] for k in keys], linestyle="dashed", marker="o", color="blue")
	plt.errorbar(keys, [res1[k][0] for k in keys], yerr=[res1[k][1] for k in keys],
		linestyle="dashed", marker="o", color="green")
	plt.errorbar(keys, [res2[k][0] for k in keys], yerr=[res2[k][1] for k in keys],
		linestyle="dashed", marker="o", color="blue")
	plt.errorbar(keys, [res3[k][0] for k in keys], yerr=[res3[k][1] for k in keys],
		linestyle="dashed", marker="o", color="red")
	# plt.errorbar(keys, y, yerr=y_error, linestyle="None", marker="None", color="green")

	plt.legend(['1MB', '2MB', '5MB'], loc='upper left')

	plt.xlabel('Redeploy Cost')
	plt.ylabel('Execution Time (s)')
	plt.ylim(0, 50)
	plt.xlim(0, keys[0] + 10)
	plt.show()

def merge_logs(log_dir):
	if not log_dir.endswith('*'):
		log_dir = log_dir + '/*'

	files = glob(log_dir)
	lines = {}
	for f in files:
		print f
		if not f.endswith('.log'): continue

		host = f.split('/')[-1].replace('.log', '')
		print host
		with open(f) as _f:
			ls = _f.readlines()
			for l in ls:
				if l.strip() == '': continue
				date = strptime(l.split()[1], '%H:%M:%S,%f')
				if date not in lines:
					lines[date] = {}
				if host not in lines[date]:
					lines[date][host] = []
				lines[date][host].append(l)

	dates = sorted(lines.keys())
	print '{0}_merged.log'.format(log_dir.replace('/*', '').split('/')[-1])
	f = open('{0}_merged.log'.format(log_dir.replace('/*', '').split('/')[-1]), 'w+')
	for d in dates:
		hs = lines[d]
		for h in hs:
			for l in hs[h]:
				f.write('{0}\t{1}'.format(h, l))

def compare_and_draw_3(log_dir_1, home_log_dir, ew1_log_dir, prefix):
	fig, ax = plt.subplots()

	res1 = parse_log_directory(log_dir_1)[prefix]
	home_vals = parse_multiple_engines(home_log_dir)
	home_mean = stats.mean(home_vals)
	home_std = stats.standard_deviation(home_vals)

	ew1_vals = parse_multiple_engines(ew1_log_dir)
	ew1_mean = stats.mean(ew1_vals)
	ew1_std = stats.standard_deviation(ew1_vals)

	print res1
	print home_mean, home_std
	print ew1_mean, ew1_std

	keys = sorted(res1.keys(), reverse=True)
	if 500 not in keys:
		res1[500] = res1[keys[0]]
		keys = sorted(res1.keys(), reverse=True)

	keys = sorted(res1.keys(), reverse=True)

	# plt.plot(keys, [res1[k][0] for k in keys], linestyle="dashed", marker="o", color="green")
	# plt.plot(keys, [res2[k][0] for k in keys], linestyle="dashed", marker="o", color="blue")
	plt.errorbar(keys, [res1[k][0] for k in keys], yerr=[res1[k][1] for k in keys],
		linestyle="dashed", marker="o", color="green", linewidth=3)
	plt.errorbar(keys, [home_mean for k in keys], yerr=[home_std for k in keys],
		linestyle="dashed", marker="o", color="blue", linewidth=3)
	plt.errorbar(keys, [ew1_mean for k in keys], yerr=[ew1_std for k in keys],
		linestyle="dashed", marker="o", color="red", linewidth=3)
	# plt.errorbar(keys, y, yerr=y_error, linestyle="None", marker="None", color="green")

	ax.annotate(6, xy=(keys[-1],res1[keys[-1]][0]), xytext=(15, -15), ha='right',
                textcoords='offset points')
	ax.annotate(5, xy=(keys[-2],res1[keys[-2]][0]), xytext=(15, -15), ha='right',
                textcoords='offset points')
	ax.annotate(4, xy=(keys[-3],res1[keys[-3]][0]), xytext=(15, -15), ha='right',
                textcoords='offset points')
	ax.annotate(1, xy=(keys[-4],res1[keys[-4]][0]), xytext=(15, -15), ha='right',
                textcoords='offset points')
	ax.annotate(1, xy=(keys[-5],res1[keys[-5]][0]), xytext=(15, -15), ha='right',
                textcoords='offset points')
	# ax.annotate(1, xy=(keys[-6],res1[keys[-5]][0]), xytext=(15, -15), ha='right',
 #                textcoords='offset points')

	plt.legend(['Solution', 'Home', 'eu-west-1'], loc='lower right')

	plt.xlabel('Engine Overhead')
	plt.ylabel('Execution Time (s)')
	plt.ylim(0, 80)
	plt.xlim(0, keys[0] + 20)
	plt.show()

def compare_and_draw_4(log_dir_1, home_log_1, ew1_log_1,
		log_dir_2, home_log_2, ew1_log_2, prefix):
	res1 = parse_log_directory(log_dir_1)[prefix]
	home_vals_1 = parse_multiple_engines(home_log_1)
	home_mean_1 = stats.mean(home_vals_1)
	home_std_1 = stats.standard_deviation(home_vals_1)

	ew1_vals_1 = parse_multiple_engines(ew1_log_1)
	ew1_mean_1 = stats.mean(ew1_vals_1)
	ew1_std_1 = stats.standard_deviation(ew1_vals_1)

	res2 = parse_log_directory(log_dir_2)[prefix]
	home_vals_2 = parse_multiple_engines(home_log_2)
	home_mean_2 = stats.mean(home_vals_2)
	home_std_2 = stats.standard_deviation(home_vals_2)

	ew1_vals_2 = parse_multiple_engines(ew1_log_2)
	ew1_mean_2 = stats.mean(ew1_vals_2)
	ew1_std_2 = stats.standard_deviation(ew1_vals_2)

	print res1
	print home_mean_1, home_std_1
	print ew1_mean_1, ew1_std_1

	print res2
	print home_mean_2, home_std_2
	print ew1_mean_2, ew1_std_2

	keys = sorted(res1.keys(), reverse=True)
	if 500 not in keys:
		res1[500] = res1[keys[0]]
		res2[500] = res2[keys[0]]
		keys = sorted(res1.keys(), reverse=True)

	keys = sorted(res1.keys(), reverse=True)

	# plt.plot(keys, [res1[k][0] for k in keys], linestyle="dashed", marker="o", color="green")
	# plt.plot(keys, [res2[k][0] for k in keys], linestyle="dashed", marker="o", color="blue")
	plt.errorbar(keys, [res1[k][0] for k in keys], yerr=[res1[k][1] for k in keys],
		linestyle="dashed", marker="o", color="green", linewidth=3)
	plt.errorbar(keys, [home_mean_1 for k in keys], yerr=[home_std_1 for k in keys],
		linestyle="dashed", marker="x", color="red", linewidth=3)
	plt.errorbar(keys, [ew1_mean_1 for k in keys], yerr=[ew1_std_1 for k in keys],
		linestyle="dashed", marker="+", color="cyan", linewidth=3)

	plt.errorbar(keys, [res2[k][0] for k in keys], yerr=[res2[k][1] for k in keys],
		linestyle="dashed", marker="o", color="blue", linewidth=3)
	plt.errorbar(keys, [home_mean_2 for k in keys], yerr=[home_std_2 for k in keys],
		linestyle="dashed", marker="x", color="magenta", linewidth=3)
	plt.errorbar(keys, [ew1_mean_2 for k in keys], yerr=[ew1_std_2 for k in keys],
		linestyle="dashed", marker="+", color="black", linewidth=3)

	plt.legend(['1 MB Solution', '1 MB Home', '1 MB eu-west-1', '2 MB Solution', '2 MB Home', '2 MB eu-west-1'],
		loc='upper right')

	plt.xlabel('Redeploy Cost')
	plt.ylabel('Execution Time (s)')
	plt.ylim(0, max([home_mean_1, home_mean_2, ew1_mean_1, ew1_mean_2]) + 5)
	plt.xlim(0, keys[0] + 10)
	plt.show()

# elr.compare_and_draw_4('../experiment_logs/2014.04.09/*', '../experiment_logs/2014.04.11/home/1MB/large01/large01', '../experiment_logs/2014.04.11/ew/1MB/large01/large01', '../experiment_logs/2014.04.10/*', '../experiment_logs/2014.04.11/home/2MB/large01/large01', '../experiment_logs/2014.04.11/ew/2MB/large01/large01', 'large01')
# elr.compare_and_draw_4('../experiment_logs/2014.04.09/*', '../experiment_logs/2014.04.11/home/1MB/large02/large02', '../experiment_logs/2014.04.11/ew/1MB/large02/large02', '../experiment_logs/2014.04.10/*', '../experiment_logs/2014.04.11/home/2MB/large02/large02', '../experiment_logs/2014.04.11/ew/2MB/large02/large02', 'large02')
# elr.compare_and_draw_4('../experiment_logs/2014.04.09/*', '../experiment_logs/2014.04.11/home/1MB/large03/large03', '../experiment_logs/2014.04.11/ew/1MB/large03/large03', '../experiment_logs/2014.04.10/*', '../experiment_logs/2014.04.11/home/2MB/large03/large03', '../experiment_logs/2014.04.11/ew/2MB/large03/large03', 'large03')
# elr.compare_and_draw_4('../experiment_logs/2014.04.09/*', '../experiment_logs/2014.04.11/home/1MB/large04/large04', '../experiment_logs/2014.04.11/ew/1MB/large04/large04', '../experiment_logs/2014.04.10/*', '../experiment_logs/2014.04.11/home/2MB/large04/large04', '../experiment_logs/2014.04.11/ew/2MB/large04/large04', 'large04')
# elr.compare_and_draw_4('../experiment_logs/2014.04.09/*', '../experiment_logs/2014.04.11/home/1MB/large05/large05', '../experiment_logs/2014.04.11/ew/1MB/large05/large05', '../experiment_logs/2014.04.10/*', '../experiment_logs/2014.04.11/home/2MB/large05/large05', '../experiment_logs/2014.04.11/ew/2MB/large05/large05', 'large05')
# elr.compare_and_draw_4('../experiment_logs/2014.04.09/*', '../experiment_logs/2014.04.11/home/1MB/large06/large06', '../experiment_logs/2014.04.11/ew/1MB/large06/large06', '../experiment_logs/2014.04.10/*', '../experiment_logs/2014.04.11/home/2MB/large06/large06', '../experiment_logs/2014.04.11/ew/2MB/large06/large06', 'large06')
# elr.compare_and_draw_4('../experiment_logs/2014.04.09/*', '../experiment_logs/2014.04.11/home/1MB/large07/large07', '../experiment_logs/2014.04.11/ew/1MB/large07/large07', '../experiment_logs/2014.04.10/*', '../experiment_logs/2014.04.11/home/2MB/large07/large07', '../experiment_logs/2014.04.11/ew/2MB/large07/large07', 'large07')
# elr.compare_and_draw_4('../experiment_logs/2014.04.09/*', '../experiment_logs/2014.04.11/home/1MB/large08/large08', '../experiment_logs/2014.04.11/ew/1MB/large08/large08', '../experiment_logs/2014.04.10/*', '../experiment_logs/2014.04.11/home/2MB/large08/large08', '../experiment_logs/2014.04.11/ew/2MB/large08/large08', 'large08')
# elr.compare_and_draw_4('../experiment_logs/2014.04.09/*', '../experiment_logs/2014.04.11/home/1MB/large09/large09', '../experiment_logs/2014.04.11/ew/1MB/large09/large09', '../experiment_logs/2014.04.10/*', '../experiment_logs/2014.04.11/home/2MB/large09/large09', '../experiment_logs/2014.04.11/ew/2MB/large09/large09', 'large09')
# elr.compare_and_draw_4('../experiment_logs/2014.04.09/*', '../experiment_logs/2014.04.11/home/1MB/large10/large10', '../experiment_logs/2014.04.11/ew/1MB/large10/large10', '../experiment_logs/2014.04.10/*', '../experiment_logs/2014.04.11/home/2MB/large10/large10', '../experiment_logs/2014.04.11/ew/2MB/large10/large10', 'large10')


 # elr.compare_and_draw_3('../experiment_logs/2014.04.10/*', '../experiment_logs/2014.04.11/home/2MB/large01/large01', '../experiment_logs/2014.04.11/ew/2MB/large01/large01', 'large01')

 # elr.compare_and_draw_3('../experiment_logs/2014.04.10/*', '../experiment_logs/2014.04.11/home/2MB/large02/large02', '../experiment_logs/2014.04.11/ew/2MB/large02/large02', 'large02')

 # elr.compare_and_draw_3('../experiment_logs/2014.04.10/*', '../experiment_logs/2014.04.11/home/2MB/large03/large03', '../experiment_logs/2014.04.11/ew/2MB/large03/large03', 'large03')

 # elr.compare_and_draw_3('../experiment_logs/2014.04.10/*', '../experiment_logs/2014.04.11/home/2MB/large04/large04', '../experiment_logs/2014.04.11/ew/2MB/large04/large04', 'large04')

 # elr.compare_and_draw_3('../experiment_logs/2014.04.10/*', '../experiment_logs/2014.04.11/home/2MB/large05/large05', '../experiment_logs/2014.04.11/ew/2MB/large05/large05', 'large05')

 # elr.compare_and_draw_3('../experiment_logs/2014.04.10/*', '../experiment_logs/2014.04.11/home/2MB/large06/large06', '../experiment_logs/2014.04.11/ew/2MB/large06/large06', 'large06')

 # elr.compare_and_draw_3('../experiment_logs/2014.04.10/*', '../experiment_logs/2014.04.11/home/2MB/large07/large07', '../experiment_logs/2014.04.11/ew/2MB/large07/large07', 'large07')

 # elr.compare_and_draw_3('../experiment_logs/2014.04.10/*', '../experiment_logs/2014.04.11/home/2MB/large08/large08', '../experiment_logs/2014.04.11/ew/2MB/large08/large08', 'large08')

 # elr.compare_and_draw_3('../experiment_logs/2014.04.10/*', '../experiment_logs/2014.04.11/home/2MB/large09/large09', '../experiment_logs/2014.04.11/ew/2MB/large09/large09', 'large09')

 # elr.compare_and_draw_3('../experiment_logs/2014.04.10/*', '../experiment_logs/2014.04.11/home/2MB/large10/large10', '../experiment_logs/2014.04.11/ew/2MB/large10/large10', 'large10')




# elr.compare_and_draw_3('../experiment_logs/2014.04.09/*', '../experiment_logs/2014.04.11/home/1MB/large01/large01', '../experiment_logs/2014.04.11/ew/1MB/large01/large01', 'large01')

 # elr.compare_and_draw_3('../experiment_logs/2014.04.09/*', '../experiment_logs/2014.04.11/home/1MB/large02/large02', '../experiment_logs/2014.04.11/ew/1MB/large02/large02', 'large02')

 # elr.compare_and_draw_3('../experiment_logs/2014.04.09/*', '../experiment_logs/2014.04.11/home/1MB/large03/large03', '../experiment_logs/2014.04.11/ew/1MB/large03/large03', 'large03')
 # elr.compare_and_draw_3('../experiment_logs/2014.04.09/*', '../experiment_logs/2014.04.11/home/1MB/large04/large04', '../experiment_logs/2014.04.11/ew/1MB/large04/large04', 'large04')

 # elr.compare_and_draw_3('../experiment_logs/2014.04.09/*', '../experiment_logs/2014.04.11/home/1MB/large05/large05', '../experiment_logs/2014.04.11/ew/1MB/large05/large05', 'large05')

 # elr.compare_and_draw_3('../experiment_logs/2014.04.09/*', '../experiment_logs/2014.04.11/home/1MB/large06/large06', '../experiment_logs/2014.04.11/ew/1MB/large06/large06', 'large06')
 # elr.compare_and_draw_3('../experiment_logs/2014.04.09/*', '../experiment_logs/2014.04.11/home/1MB/large07/large07', '../experiment_logs/2014.04.11/ew/1MB/large07/large07', 'large07')

 # elr.compare_and_draw_3('../experiment_logs/2014.04.09/*', '../experiment_logs/2014.04.11/home/1MB/large08/large08', '../experiment_logs/2014.04.11/ew/1MB/large08/large08', 'large08')

 # elr.compare_and_draw_3('../experiment_logs/2014.04.09/*', '../experiment_logs/2014.04.11/home/1MB/large09/large09', '../experiment_logs/2014.04.11/ew/1MB/large09/large09', 'large09')

 # elr.compare_and_draw_3('../experiment_logs/2014.04.09/*', '../experiment_logs/2014.04.11/home/1MB/large10/large10', '../experiment_logs/2014.04.11/ew/1MB/large10/large10', 'large10')
