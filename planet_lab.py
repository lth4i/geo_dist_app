import subprocess
import os
import xmlrpclib

import deployment

def get_nodes(file='pl_files/nodes.txt'):
	with open(file) as f:
		lines = f.readlines()
	return [l.strip() for l in lines if not l.strip().startswith('#') and not l.strip() == '']

def get_auth():
	auth = {}
	auth['Username'] = os.environ['PL_USERNAME']
	auth['AuthString'] = os.environ['PL_AUTH_STRING']
	auth['AuthMethod'] = 'password'
	return auth

def get_roles():
	auth = get_auth()
	api = xmlrpclib.ServerProxy('https://www.planet-lab.eu/PLCAPI/')
	return api.GetRoles(auth)

def cp_pip_installer(username, host):
	deployment.scp(username, host, file_name='pl_files/get-pip.py')

def ping_test():
	try:
		nodes = get_nodes()
	except IOError:
		nodes = get_nodes('nodes.txt')

	for node in nodes:
		if len(node.split()) == 2:
			host = node.split()[1]
		else:
			host = node

		try:
			call = subprocess.Popen(['ping', '-c', '10', host], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			ping_output = subprocess.check_output(['tail', '-1'], stdin=call.stdout)
			ping_result = ping_output.split()[-2]
			latency = float(ping_result.split('/')[-3])
			standard_deviation = float(ping_result.split('/')[-1])
			print host, latency, standard_deviation
		except:
			print 'Failed to ping {0}'.format(host)

def check_app_installed(username, host, app_name):
	call = subprocess.Popen(['ssh', '-i', '/Users/longthai/.ssh/planetlab-key',
			'{0}@{1}'.format(username, host), 'which {0}'.format(app_name)],
		stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	errors = call.stderr.readlines()
	if len(errors) > 0 and 'Permanently added' not in errors[0]:
		for e in errors:
			if 'no pip in' in e:
				return False
		raise Exception(errors)
	return len(call.stdout.readlines()) != 0

def get_slice():
	auth = get_auth()
	api = xmlrpclib.ServerProxy('https://www.planet-lab.eu/PLCAPI/')
	return api.GetSlices(auth)[0]

def get_node_by_id(node_id, fields=['hostname']):
	if type(node_id) != int:
		node_id = int(node_id)

	auth = get_auth()
	api = xmlrpclib.ServerProxy('https://www.planet-lab.eu/PLCAPI/')
	nodes = api.GetNodes(auth, {'node_id':node_id}, fields)
	if len(nodes) == 0:
		return None
	elif len(nodes) > 1:
		raise Exception('Nodes [{0}] share the same node id {1}'.format(nodes, node_id))
	return nodes[0]

def get_known_nodes():
	known_node_ids = get_slice()['node_ids']
	known_nodes = []
	for node_id in known_node_ids:
		node_name = get_node_by_id(node_id, fields=['hostname'])
		known_nodes.append(node_name['hostname'])

	return known_nodes

def get_unknown_nodes():
	nodes = get_nodes()

	known_nodes = get_known_nodes()

	unknown_nodes = []
	for node in nodes:
		if len(node.split()) == 2:
			node = node.split()[0]
		if node not in known_nodes:
			unknown_nodes.append(node)

	return unknown_nodes

def add_slice_to_nodes(nodes=None):
	if nodes == None:
		nodes = get_unknown_nodes()

	print 'Adding slice staple_together to nodes: {0}'.format(nodes)

	auth = get_auth()
	api = xmlrpclib.ServerProxy('https://www.planet-lab.eu/PLCAPI/')
	result = api.AddSliceToNodes(auth, 'staple_together', nodes)

	return result