import matplotlib.pyplot as plt
import numpy as np

def compare_theo_prac(wf):
	with open("experiment_result") as f:
		lines = [l.strip() for l in f.readlines() if wf in l and 'nearest' not in l and 'home' not in l]
	
	exec_times = {}
	costs = {}
	for l in lines:
		tokens = l.split()
		num_eng = int(tokens[8])
		exec_time = float(tokens[-2])
		exec_times[num_eng] = exec_time

		_tokens = tokens[7].split('/')[-1].replace('large', 'larger').split('_')
		solution_name = 'rtt_{0}_0{1}.solution'.format(_tokens[0], _tokens[1])
		with open('../cloudsconstraints/workflow-deployment/outputs/{0}'.format(solution_name)) as f:
			cost = [l for l in f.readlines() if 'totalCostMovement' in l][0].split()[-1]
			costs[num_eng] = float(cost)

	speedups = {}
	for num_eng in exec_times:
		if num_eng == 1: continue
		speedups[num_eng] = (exec_times[1] / exec_times[num_eng], costs[1] / costs[num_eng])
		# print num_eng, exec_times[1] / exec_times[num_eng], costs[1] / costs[num_eng]

	print speedups

	theory = []
	actual = []
	for n in sorted(speedups.keys()):
		theory.append(speedups[n][1])
		actual.append(speedups[n][0])

	print theory
	print actual

	ind = np.arange(len(speedups))  # the x locations for the groups
	width = 0.35
	fig, ax = plt.subplots()

	rects1 = ax.bar(ind, theory, width, color='r')
	rects2 = ax.bar(ind+width, actual, width, color='b')
	ax.legend( (rects1[0], rects2[0]), ('Theory', 'Actual') )

	plt.ylim(0, 2)
	plt.xlim(0, 4)
	ax.set_xticks(ind+width)
	ax.set_xticklabels(sorted(speedups.keys()))

	# def autolabel(rects):
 #    # attach some text labels
	# 	for rect in rects:
	# 		height = rect.get_height()
	# 		print height
	# 		ax.text(rect.get_x()+rect.get_width()/2., 1.05*height, '%d'%float(height),
	# 				ha='center', va='bottom')

	# autolabel(rects1)
	# autolabel(rects2)

	plt.show()