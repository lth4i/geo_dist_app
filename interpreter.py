from dom.service_dom import Service_Dom
from dom.host_dom import Host_Dom

def parse_token_if_required(string, delimiter=','):
	tokens = [t.strip() for t in string.split(delimiter)]
	
	if len(tokens) == 1:	# return a token if there is only one
		return tokens[0]
	else:					# return list of tokens if there are more than one
		return tokens

def interpret_script_file(name):
	with open(name) as f:
		lines = f.readlines()

	return interpret(lines)

def interpret(lines):
	services = {}
	hosts = {}
	deployment_plan = {}
	engines = {}

	for line in lines:
		if line.strip() == '': continue # ignore empty line

		tokens = [t.strip() for t in line.split()]

		# check tokens
		if len(tokens) == 0:
			raise Exception('Invalid line: {0}'.format(line))

		# check first token which indicates the function
		if tokens[0] == '#': # for comment
			continue
		elif tokens[0] == 'serv':
			# serv Producer services/producer.py producer.Producer
			if len(tokens) != 4:
				raise Exception('Invalid service script {0}'.format(line))

			service_name = tokens[1]									# 2nd token is service name
			service_script = [t.strip() for t in tokens[2].split(',')]	# 3rd token is service script(s)
			service_class = tokens[3]									# 4th token is service class
			service = Service_Dom(service_name, service_script, service_class)

			services[service_name] = service
		elif tokens[0] == 'host':
			# 1st token: action
			# 2nd token: alias
			# 3rd token: region
			# 4th token: username
			# 5th token: ip
			# host home home longthai localhost
			# home_host_dom = Host_Dom('home', 'longthai', 'localhost')
			if len(tokens) != 5:
				raise Exception('Invalid host script {0}'.format(line))

			host_name = tokens[1]
			host_region = tokens[2]
			host_username = tokens[3]
			host_ip = tokens[4]
			host = Host_Dom(host_name, host_region, host_username, host_ip)

			hosts[host_name] = host
		elif tokens[0] == 'depl':
			# depl home Producer,Consumer,Aggregator
			# 'home':[producer_service_dom, consumer_service_dom, aggregator_service_dom]
			if len(tokens) != 3:
				raise Exception('Invalid deployment script {0}'.format(line))

			host = tokens[1]
			deployed_services = [t.strip() for t in tokens[2].split(',')]

			# check if all services are known
			for service in deployed_services:
				if service not in services:
					raise Exception('Unknown service {0} in deployment script {1}'.format(service, line))

			deployment_plan[host] = deployed_services
		elif tokens[0] in hosts:	# if first token is a host, it's a engine
			engine = tokens[0]
			if engine not in engines: engines[engine] = []
			# > home.Consumer 1024 $d1
			# 1024 > home.Consumer > $d1
			if len(tokens) != 4:
				raise Exception('Invalid execution script {0}'.format(line))

			service = tokens[1]
			params = tokens[2]
			output = tokens[3]
			string = '{0} {1} {2}'.format(service, params, output)

			engines[engine].append(string)
		else:
			raise Exception('Invalid line: {0}'.format(line))

	return services, hosts, deployment_plan, engines