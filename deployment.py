import subprocess
import time

from dom.service_dom import Service_Dom

"""
"""
def execute_command(command, return_result=True):
	if len(command) == 0: raise ValueError('Empty command')

	call = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

	# TODO: calling this will make an application hang, find another way to check error
	if return_result:
		errors = call.stderr.readlines()
		if len(errors) > 0:
			if 'Permanently added' not in errors[0]:
				raise Exception(str(errors))

	if return_result:
		return call.stdout.readlines()
	else: return None

"""
"""
def scp(username='long', host='138.251.195.171',
		directory='~/', file_name='dummy', destination='.', return_result=True):
	return execute_command(['scp', file_name,
		'{0}@{1}:{2}/'.format(username, host, directory)], return_result)

"""
"""
def execute_remote_command(username, host, command_string, return_result=True):
	if command_string.strip() == '': raise ValueError('Empty command')
	return execute_command(['ssh', '{0}@{1}'.format(username, host), command_string, '&'],
		return_result)

"""
"""
def do_deployment(username, host, services, ignore_running=False):
	service_folder = 'services'

	# check if service is already running
	try:
		service_id = get_service_id(username, host)
		if ignore_running:
			print 'Service {0} already running on host {1}'.format(service_id, host)
			return
		print 'Service {0} already running on host {1}. Terminate it NOW'.format(service_id, host)
		terminate_deployment(username, host) # terminate existing service if there is any
		time.sleep(1) # (hackily) sleep for 1 second to avoid race condition
	except Exception:
		pass

	for i in range(5):
		try:
			# create services folder
			execute_remote_command(username, host, command_string='mkdir {0}'.format(service_folder))

			# copy service scripts
			copied_files = []	# keep track of already copied files to save time
			for service in services:
				for script_file in service.script_files:
					if script_file not in copied_files:
						scp(username, host, '~/{0}'.format(service_folder), script_file)
						copied_files.append(script_file)
			# copy service_runner.py
			scp(username, host, '~/{0}'.format(service_folder), 'service_runner.py')

			# run service
			service_classes = ''
			for service in services:
				service_classes = service_classes + service_folder + '.' + service.class_name + ' '
			command_string = 'python {0}/service_runner.py {1}'.format(service_folder, service_classes.strip())
			print command_string
			execute_remote_command(username, host, command_string, return_result=False) # don't return result, otherwise an application will hang

			time.sleep(1) # sleep for 1 second to avoid race condition
			service_id = get_service_id(username, host)

			print 'Service is running on port 8080 at host {0} with process id {1}'.format(host, service_id)
			break
		except Exception, e:
			print 'Encounter exception {0}. Will retry shortly'.format(e)
			time.sleep(5)
			continue

"""
"""
def terminate_deployment(username='long', host='138.251.195.171'):
	service_folder = 'services'

	# stop services
	service_id = get_service_id(username, host)
	print 'Found python service running on port 8080 of host {0} with process id {1}'.format(host, service_id)
	execute_remote_command(username, host, 'kill -9 {0}'.format(service_id))

	# delete service scripts
	execute_remote_command(username, host, 'rm -rf {0}'.format(service_folder))

"""
"""
def get_service_id(username='long', host='138.251.195.171', port=8080):
	for i in range(5):
		call_result = execute_remote_command(username, host,
			command_string='lsof -i -P | grep 8080 | grep python | grep LISTEN')
		if call_result == None or len(call_result) == 0:
			time.sleep(1)
		else: 
			return call_result[0].split()[1]

	raise Exception('Cannot find any python service running on port {0} of host {1}'.format(port, host))

def test():
	service_dom = Service_Dom('Metric_Calculator',
		['metric_collector/metric_collector.py', 'metric_collector/round_trip_time.py',
			'metric_collector/bandwidth.py', 'metric_collector/stats.py'],
		'metric_collector.Metric_Collector')
	do_deployment(services=[service_dom])