import subprocess
import threading
from threading import Thread
import logging

# init logger
formatter = logging.Formatter(fmt='%(asctime)s - %(levelname)s - %(module)s - %(message)s')
handler = logging.FileHandler('./ping.log')
handler.setFormatter(formatter)
logger = logging.getLogger("round_trip_time")
logger.setLevel(logging.DEBUG)
logger.addHandler(handler)

def ping(host, num_tests=10):
	# get latency using ping
	tmp_ping_call = subprocess.Popen(['ping', '-c', str(num_tests), host], stdout=subprocess.PIPE)
	ping_output = subprocess.check_output(['tail', '-1'], stdin=tmp_ping_call.stdout)
	ping_result = ping_output.split()[-2]
	latency = float(ping_result.split('/')[-3])
	standard_deviation = float(ping_result.split('/')[-1])

	return latency, standard_deviation

def do_ping(host, num_tests=10):
	try:
		l, std = ping(host, num_tests)
		print host, l, std
		logger.info('{0} {1} {2}'.format(host, l, std))
	except Exception, e:
		print 'Failed to ping {0}'.format(host)
		logger.exception('Failed to ping {0}'.format(host))

def ping_hosts(file_name='hosts'):
	with open(file_name) as f:
		lines = [l.strip() for l in f.readlines() if not l.strip().startswith('#') and not l.strip() == '']


	for line in lines:
		if len(line.split()) != 1:
			name = line.split()[0]
			host = line.split()[1]
		else:
			name = line
			host = line

		t = Thread(target=do_ping, args=(host, 10))
		t.start()

	print 'DONE'

if __name__ == '__main__':
	ping_hosts()
