import bandwidth
import round_trip_time

class Metric_Collector:
	exposed = True

	def GET(self, host):
		try:
			bw_mean, bw_std = bandwidth.use_iperf(host)
			rtt_mean, rtt_std = round_trip_time.ping(host)

			results = {}

			results['bw_mean'] = bw_mean
			results['bw_std'] = bw_std
			results['rtt_mean'] = rtt_mean
			results['rtt_std'] = rtt_std

			return str(results)
		except Exception as e:
			return e.message