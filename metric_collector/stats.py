'''
TEMPORARILY put statistic functions here
This should be later moved to a specialised project
'''


import operator

'''
Find the mean (i.e. average) value of a set
'''
def mean(s):
	return float(sum(s)) / len(s)

'''
Find the median, i.e. the value which is larger than 50% of values of the set
'''
def median(s):
	return percentile(s, 50) # median is the 50th percentile

'''
Find percentile using nearest rank method
'''
def percentile(s, p):
	r = (float(p) / 100) * len(s) + 1 / 2 # add 1/2 in order to make sure the rank is the least integer with is larger p-th rank
	return sorted(s)[int(p)]

'''
Find the median, the most occuring values, of the set.
Noted: there may be more than 1 mode in a given set
'''
def mode(s):
	counts = {}
	reversed_count = {}
	# count occurence of each value
	for item in s:
		if item not in counts: counts[item] = 1
		else: counts[item] = counts[item] + 1
	
	# sort by occurence in decending order
	sorted_counts = sorted(counts.items(), key=operator.itemgetter(1), reverse=True)
	results = []
	max_count = 0
	for count in sorted_counts:
		if count[1] >= max_count:
			max_count = count[1]
			results.append(count[0])
		else: break
	return results

def variance(s):
	m = mean(s)
	v = 0
	for item in s:
		v = v + pow(item - m, 2)
	v = v / float(len(s))
	return v

def standard_deviation(s):
	return pow(variance(s), 0.5)
