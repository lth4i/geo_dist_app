import subprocess
import os
import time

import stats

import json

import logging
logger = logging.getLogger('bandwidth')
hdlr = logging.FileHandler('bandwidth.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s: %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr) 
logger.setLevel(logging.INFO)

"""
Find bandwidth using scp
"""
def use_scp(username='long', host='138.251.195.171',
		directory='~/', file_name='dummy', destination='.', file_size=100,
		num_tests=1):
	bandwidths = []
	for i in range(num_tests):
		comm = 'scp {0}@{1}:{2}/{3} {4}'.format(username, host, directory, file_name, destination)	
		start = time.time()
		os.system(comm)		# copy file
		end = time.time()
		print (end - start)
		bandwidths.append(file_size / (end - start))

		try:
			os.remove('{0}/{1}'.format(destination, file_name)) # remove the copied file
		except OSError:
			raise Exception('Faied to connect to {0}'.format(host)) # hacky exception handling
	return stats.mean(bandwidths), stats.standard_deviation(bandwidths)

"""
Find bandwidth using curl
"""
def use_curl(username='long', host='138.251.195.171',
		file_name='pg11.txt', destination='.',
		num_tests=10):

	bandwidths = []
	for i in range(num_tests):
		call = subprocess.Popen(['curl', '-w', '%{size_download} %{time_total}',
			host + '/' + file_name, '-o', destination + '/' + file_name],
			stdout=subprocess.PIPE, stderr=subprocess.PIPE)

		curl_output = subprocess.check_output(['tail', '-1'], stdin=call.stdout)
		print curl_output
		size, time = curl_output.split()
		bandwidths.append(float(size) / float(time) / (1024 ** 2))	# convert to mb/s

		try:
			os.remove('{0}/{1}'.format(destination, file_name)) # remove the copied file
		except OSError:
			raise Exception('Faied to connect to {0}'.format(host)) # hacky exception handling

	return stats.mean(bandwidths), stats.standard_deviation(bandwidths)

"""
Find bandwidth using iperf
"""
def use_iperf(host='138.251.195.171', port=8080, test_interval=2, timing=10):
	global logger

	call = subprocess.Popen(['iperf', '-c', host, '-p', str(port),
			'-i', str(test_interval), '-t', str(timing), '-f', 'MBytes'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	
	errors = call.stderr.readlines()
	if len(errors) > 0:
		raise Exception(errors[0])

	lines = call.stdout.readlines()
	size = len(lines)
	res = lines[size - int(timing / test_interval) - 1:size]
	bandwidths = []
	for i in range(0, len(res) - 1):	# ignore last line as it is the summary one
		logger.info('To {0}: {1}'.format(host, res[i]))
		bandwidths.append(float(res[i].split()[-2]))

	bw_m = stats.mean(bandwidths)
	bw_std = stats.standard_deviation(bandwidths)

	logger.info('To {0}: bw_m = {1}, bw_std = {2}'.format(host, bw_m, bw_std))

	return bw_m, bw_std

class Bandwidth_Collector:
	exposed = True

	def GET(self, host):
		try:
			return use_iperf(host)
		except Exception as e:
			return e.message