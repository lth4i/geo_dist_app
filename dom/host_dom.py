class Host_Dom:
	def __init__(self, name, region, username, host):
		self.name = name
		self.region = region
		self.username = username
		self.host = host

	def __str__(self):
		return '[Host_Dom: {0}, {1}, {2}, {3}]'.format(self.name, self.region,
			self.username, self.host)

	def __repr__(self):
		return self.__str__()