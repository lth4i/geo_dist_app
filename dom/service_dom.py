"""
Domain Object Model of a service.
This entity includes service name, its script file and class name (used for instantiation)
"""
class Service_Dom:
	def __init__(self, name, script_files, class_name):
		self.name = name
		self.class_name = class_name
		self.script_files = script_files

	def __str__(self):
		return '[Service_Dom: {0}, {1}, {2}]'.format(self.name,
			self.script_files, self.class_name)

	def __repr__(self):
		return self.__str__()