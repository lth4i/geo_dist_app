import subprocess
import time
import datetime
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

def get_log_lines(location, starter, additional_filter=None):
	comm = "egrep 'Start|take|setting' {0}/* | grep -vi setter | grep -vi producer".format(location)
	call = subprocess.Popen(comm, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
	lines = [l.strip() for l in call.stdout.readlines()]

	cache = {}
	for l in lines:
		t1, t2 = l.split(':', 1)
		if '/' in t1:
			file_name = t1.split('/')[-1]
		else:
			file_name = t1

		new_line = '{0}:{1}'.format(file_name.replace('.log', ''), t2)
		
		time = datetime.datetime.strptime(t2.split()[1], '%H:%M:%S,%f')
		# print str(time)
		if time not in cache: cache[time] = []
		cache[time].append(new_line)
	times = sorted(cache.keys())
	for t in times:
		# print t
		for l in cache[t]:
			t1, t2 = l.split(':', 1)
			if starter != None and t1 == starter and 'Start' in t2:
				start = datetime.datetime.strptime(t2.split()[1], '%H:%M:%S,%f')
			if starter != None and t1 != starter and 'Start' in t2: continue
			if additional_filter != None and additional_filter not in l: continue
			end = datetime.datetime.strptime(t2.split()[1], '%H:%M:%S,%f')
			print t1, '\t', (end - start).total_seconds(), '\t', t2

def time_diff(start, end):
	time_start = datetime.datetime.strptime(start, '%H:%M:%S,%f')
	time_end = datetime.datetime.strptime(end, '%H:%M:%S,%f')
	print time_start
	print time_end
	return (time_end - time_start).total_seconds()

def get_default_solutions(name, wf):
	with open(name) as f:
		lines = [l.strip() for l in f.readlines() if l.strip() != '' and ('home' in l or 'nearest' in l)]

	cache = {}
	for l in lines:
		tokens = l.split('\t')
		names = tokens[0]
		if '/' in names: names = names.split('/')[-1]
		ts = names.split('_')
		if len(ts) == 2:
			wf_name, wf_sol = names.split('_')
		else:
			wf_name = ts[0] + '_' + ts[1]
			wf_sol = ts[2]
		if wf_name not in cache: cache[wf_name] = {}
		if wf_sol not in cache[wf_name]: cache[wf_name][wf_sol] = tokens[1:]
		else: raise Exception('Solution {0} already exists for workflow {1}'.format(wf_sol, wf_name))

	return cache[wf]

def parse_log(name, wf):
	with open(name) as f:
		lines = [l.strip() for l in f.readlines() if l.strip() != '' and 'home' not in l and 'nearest' not in l]

	cache = {}
	for l in lines:
		tokens = l.split('\t')
		names = tokens[0]
		if '/' in names: names = names.split('/')[-1]
		ts = names.split('_')
		if len(ts) == 2:
			wf_name, wf_sol = names.split('_')
		else:
			wf_name = ts[0] + '_' + ts[1]
			wf_sol = ts[2]
		wf_sol = int(wf_sol)
		if wf_name not in cache: cache[wf_name] = {}
		if wf_sol not in cache[wf_name]: cache[wf_name][wf_sol] = tokens[1:]
		else: raise Exception('Solution {0} already exists for workflow {1}'.format(wf_sol, wf_name))

	return cache[wf]

def parse_log_num(name, wf):
	with open(name) as f:
		lines = [l.strip() for l in f.readlines() if l.strip() != '' and 'home' not in l and 'nearest' not in l]

	cache = {}
	for l in lines:
		tokens = l.split('\t')
		names = tokens[0]
		if '/' in names: names = names.split('/')[-1]
		ts = names.split('_')
		if len(ts) == 2:
			wf_name, wf_sol = names.split('_')
		else:
			wf_name = ts[0] + '_' + ts[1]
			wf_sol = ts[2]
		wf_sol = int(wf_sol)
		num_engine = int(tokens[1])
		if wf_name not in cache: cache[wf_name] = {}
		if num_engine not in cache[wf_name]: cache[wf_name][num_engine] = [wf_sol] + tokens[2:]
		else: raise Exception('Solution {0} already exists for workflow {1}'.format(num_engine, wf_name))

	return cache[wf]

def draw(name, wf):

	fig, ax = plt.subplots()

	data = parse_log(name, wf)
	sols = sorted(data.keys())
	means = []
	stds = []
	for s in sols:
		means.append(float(data[s][2]))
		stds.append(float(data[s][3]))
	if 500 not in sols:
		sols.append(500)
		data[500] = data[sols[-2]]
		means.append(means[-1])
		stds.append(stds[-1])

	def_sols = get_default_solutions(name, wf)
	home_mean = float(def_sols['home'][-2])
	home_std = float(def_sols['home'][-1])
	nearest_mean = float(def_sols['nearest'][-2])
	nearest_std = float(def_sols['nearest'][-1])

	plt.errorbar(sols, means, yerr=stds,
		linestyle="dashed", marker="o", color="green", linewidth=3)
	plt.errorbar(sols, [home_mean for s in sols], yerr=[home_std for s in sols],
		linestyle="dashed", marker="o", color="blue", linewidth=3)
	plt.errorbar(sols, [nearest_mean for s in sols], yerr=[nearest_std for s in sols],
		linestyle="dashed", marker="o", color="red", linewidth=3)

	for i in range(len(sols)):
		s = sols[i]
		ax.annotate(data[s][0], xy=(s,means[i]), xytext=(15, -15), ha='right',
			textcoords='offset points')

	plt.legend(['Solution', 'Home', 'Nearest'], loc='lower right')

	plt.xlabel('Redeploy Cost')
	plt.ylabel('Execution Time (s)')
	plt.ylim(0, max(home_mean, nearest_mean) + 10)
	plt.xlim(-5, 520)
	plt.show()

def draw_num(name, wf):
	fig, ax = plt.subplots()

	data = parse_log_num(name, wf)
	sols = sorted(data.keys())
	means = []
	stds = []
	for s in sols:
		means.append(float(data[s][2]))
		stds.append(float(data[s][3]))

	def_sols = get_default_solutions(name, wf)
	home_mean = float(def_sols['home'][-2])
	home_std = float(def_sols['home'][-1])
	nearest_mean = float(def_sols['nearest'][-2])
	nearest_std = float(def_sols['nearest'][-1])

	plt.errorbar(sols, means, yerr=stds,
		linestyle="dashed", marker="o", color="green", linewidth=3)
	plt.plot(range(0, 9), [home_mean for s in range(0, 9)],
		marker=None, color="blue", linewidth=3)
	plt.plot(range(0, 9), [nearest_mean for s in range(0, 9)],
		marker=None, color="red", linewidth=3)

	for i in range(len(sols)):
		s = sols[i]
		ax.annotate(data[s][0], xy=(s,means[i]), xytext=(-10, -10), ha='right',
			textcoords='offset points')

	plt.legend(['Solution', 'Home', 'Nearest'], loc='lower right')

	plt.xlabel('Number of Engines')
	plt.ylabel('Execution Time (s)')
	plt.ylim(0, max(home_mean, nearest_mean) + 5)
	plt.xlim(0, 8)

	matplotlib.rcParams.update({'font.size': 15})
	plt.savefig('{0}_num_comp'.format(wf), bbox_inches='tight')

	# plt.show()

def draw_compare(name, wf):
	fig, ax = plt.subplots()

	rtt_wf = wf
	bw_wf = 'bw_' + wf

	rtt_data = parse_log(name, rtt_wf)
	bw_data = parse_log(name, bw_wf)

	rtt_sols = sorted(rtt_data.keys())
	rtt_means = []
	rtt_stds = []
	for s in rtt_sols:
		rtt_means.append(float(rtt_data[s][2]))
		rtt_stds.append(float(rtt_data[s][3]))
	if 500 not in rtt_sols:
		rtt_sols.append(500)
		rtt_data[500] = rtt_data[rtt_sols[-2]]
		rtt_means.append(rtt_means[-1])
		rtt_stds.append(rtt_stds[-1])

	bw_sols = sorted(bw_data.keys())
	bw_means = []
	bw_stds = []
	has_one = False
	for s in bw_sols:
		bw_means.append(float(bw_data[s][2]))
		bw_stds.append(float(bw_data[s][3]))
		if (int(bw_data[s][0]) == 1): has_one = True
	if 500 not in bw_sols:
		if has_one:
			bw_sols.append(500)
			bw_data[500] = bw_data[bw_sols[-2]]
			bw_means.append(bw_means[-1])
			bw_stds.append(bw_stds[-1])
		else:
			bw_sols.append(500)
			bw_data[500] = rtt_data[500]
			bw_means.append(rtt_means[-1])
			bw_stds.append(rtt_stds[-1])

	plt.errorbar(rtt_sols, rtt_means, yerr=rtt_stds,
		linestyle="dashed", marker="o", color="green", linewidth=3)
	plt.errorbar(bw_sols, bw_means, yerr=bw_stds,
		linestyle="dashed", marker="o", color="blue", linewidth=3)

	for i in range(len(rtt_sols)):
		s = rtt_sols[i]
		ax.annotate(rtt_data[s][0], xy=(s,rtt_means[i]), xytext=(15, -15), ha='right',
			textcoords='offset points')
	for i in range(len(bw_sols)):
		s = bw_sols[i]
		ax.annotate(bw_data[s][0], xy=(s,bw_means[i]), xytext=(15, -15), ha='right',
			textcoords='offset points')

	plt.legend(['RTT', 'Bandwidth'], loc='upper left')
	# plt.ylim(0, 50)
	plt.xlim(-5, 520)
	plt.show()

def draw_compare_num(name, wf):
	fig, ax = plt.subplots()

	rtt_wf = wf
	bw_wf = 'bw_' + wf

	rtt_data = parse_log_num(name, rtt_wf)
	bw_data = parse_log_num(name, bw_wf)

	rtt_sols = sorted(rtt_data.keys())
	rtt_means = []
	rtt_stds = []
	for s in rtt_sols:
		rtt_means.append(float(rtt_data[s][2]))
		rtt_stds.append(float(rtt_data[s][3]))

	bw_sols = sorted(bw_data.keys())
	bw_means = []
	bw_stds = []
	has_one = False
	for s in bw_sols:
		bw_means.append(float(bw_data[s][2]))
		bw_stds.append(float(bw_data[s][3]))
		if (int(bw_data[s][0]) == 1): has_one = True
	if 1 not in bw_sols:
		bw_sols.insert(0, 1)
		bw_data[1] = rtt_data[1]
		bw_means.insert(0, rtt_means[0])
		bw_stds.insert(0, rtt_stds[0])

	plt.errorbar(rtt_sols, rtt_means, yerr=rtt_stds,
		linestyle="dashed", marker="o", color="green", linewidth=3)
	plt.errorbar(bw_sols, bw_means, yerr=bw_stds,
		linestyle="dashed", marker="o", color="blue", linewidth=3)

	for i in range(len(rtt_sols)):
		s = rtt_sols[i]
		ax.annotate(rtt_data[s][0], xy=(s,rtt_means[i]), xytext=(-10, -10), ha='right',
			textcoords='offset points')
	for i in range(len(bw_sols)):
		s = bw_sols[i]
		ax.annotate(bw_data[s][0], xy=(s,bw_means[i]), xytext=(-10, -10), ha='right',
			textcoords='offset points')

	plt.xlabel('Number of Engines')
	plt.ylabel('Execution Time (s)')
	plt.legend(['RTT', 'Bandwidth'], loc='upper right')
	# plt.ylim(0, 50)
	plt.xlim(0, 8.01)
	plt.show()

def draw_bar_chart(data):
	"""
	data is a map from label to value
	"""

	labels = data.keys()
	values = data.values()

	ind = np.arange(len(labels))
	width = 0.3

	fig, ax = plt.subplots()
	rects1 = ax.bar(ind+width/2, values, width, color='r')
	ax.set_xticks(ind+width)
	ax.set_xticklabels(labels)
	print max(values)+50
	plt.ylim(0, max(values)+50)

	plt.xlabel('Solutions')
	plt.ylabel('Overral execution time (seconds)')

	def autolabel(rects):
	# attach some text labels
		for rect in rects:
			height = rect.get_height()
			ax.text(rect.get_x()+rect.get_width()/2., 1.05*height, '%d'%int(height),
					ha='center', va='bottom')

	autolabel(rects1)

	plt.xlim(0, 2)

	plt.show()


