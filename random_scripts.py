import planet_lab as pl
import deployment

from random import randint
from cloud_manager import aws_manager
from metric_collector import stats

from threading import Thread
import xmlrpclib

import re

from glob import glob

def install_pip():
	kn = pl.get_known_nodes()
	for n in kn:
		ts = []
		try:
			t = Thread(target=check_and_install, args=(n,))
			t.start()
			ts.append(t)
		except Exception, e:
			print n, e

def check_and_install(n):
	try:
		c = pl.check_app_installed('staple_together', n, 'pip')
		if not c:
			print 'Copying pip installer to {0}'.format(n)
			pl.cp_pip_installer('staple_together', n)
			print 'Finish copying pip installer to {0}. Start installing'.format(n)
			deployment.execute_remote_command('staple_together', n, 'python get-pip.py')
			print 'Finish installing pip at {0}'.format(n)
		else:
			print 'Pip is already installed at {0}'.format(n)
	except Exception, e:
		print n, e

def delete_faulty_nodes():
	kn = pl.get_known_nodes()
	nodes = []
	for n in kn:
		try:
			pl.check_app_installed('staple_together', n, 'pip')
		except:
			print 'Node {0} is faulty'.format(n)
			nodes.append(n)

	auth = pl.get_auth()
	api = xmlrpclib.ServerProxy('https://www.planet-lab.eu/PLCAPI/')
	res = api.DeleteSliceFromNodes(auth, 'staple_together', nodes)
	return res

def generate_ws(num):
	rs = aws_manager.cherrypy_images.keys()
	while True:
		l = []
		for j in range(num):
			l.append(rs[randint(0, len(rs) - 1)])

		print len(set(l)), num, len(rs)
		if len(set(l)) == min(num, len(rs)):
			for j in range(len(l)):
				print 'ws{0}: {1}'.format(j + 1, l[j])
			break

def parse_invo_test_log(name):
	with open(name) as f:
		lines = [l.strip() for l in f.readlines() if l.strip() != '']

	cache = {}
	times = {}

	for l in lines:
		if l.startswith('Replacing'):
			ts = l.split()
			ip = ts[-1]
			name = ts[1]

			if ip not in cache:
				# print ip, name
				cache[ip] = name
			else: 
				if name != cache[ip]:
					raise Exception('Ip {0} has name {1} and {2}'.format(ip, cache[i], name))
		elif 'take' in l:
			# 2014-05-10 17:19:24,948 - INFO - engine - Invoking [54.76.2.152, Engine_Data_Producer, 'data':1048576, $d0, INVOKED] take 0.29505610466 seconds
			ts = l.split()
			ip = ts[8].replace('[', '').replace(',', '')
			name = cache[ip]
			t = float(ts[-2])

			if name not in times: times[name] = []
			times[name].append(t)

	for ts in times:
		print ts, stats.mean(times[ts]), stats.standard_deviation(times[ts])

def create_data_cleaning_steps(file_location):
	with open(file_location) as f:
		lines = [l.strip() for l in f.readlines() if l.strip() != '' and not l.strip().startswith(("#", "serv", "depl", "host"))]

	cache = {}
	for l in lines:
		tokens = l.split()
		if len(tokens) <= 2: continue

		engine = tokens[0]
		if engine not in cache: cache[engine] = []
		output = tokens[3]
		_inputs = tokens[2].split(',')
		inputs = []
		for i in _inputs:
			_is = i.split(':')
			for _i in _is:
				if "'" not in _i:
					try:
						float(_i)
					except Exception:
						inputs.append(_i)
		if len(inputs) > 0 and output != '$0':
			cache[engine].append((inputs, output))
			print engine, inputs, output

	results = {}
	for e in cache:
		results[e] = {}
		for p in cache[e]:
			inputs = p[0]
			output = p[1]
			for i in inputs:
				if i not in results[e]: results[e][i] = []
				if output not in results[e][i]: results[e][i].append(output)
	return results