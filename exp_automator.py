import deployment
import executor
import os
import sys
import subprocess
import time
from cloud_manager import aws_manager
import datetime
from metric_collector import stats
import logging

# init logger
formatter = logging.Formatter(fmt='%(asctime)s - %(levelname)s - %(module)s - %(message)s')
handler = logging.FileHandler('./experiment.log')
handler.setFormatter(formatter)
logger = logging.getLogger("experiment")
logger.setLevel(logging.INFO)
logger.addHandler(handler)

def automate_experiments():
	try:
		# automate_experiment('experiments/pl_tests/pl_eu-west-1', 'en', 'en', 20)
		# automate_experiment('experiments/pl_tests/pl_ap-northeast-1', 'en', 'en', 20)
		# automate_experiment('experiments/pl_tests/pl_ap-southeast-1', 'en', 'en', 20)
		# automate_experiment('experiments/pl_tests/pl_ap-southeast-2', 'en', 'en', 20)
		# automate_experiment('experiments/pl_tests/pl_sa-east-1', 'en', 'en', 20)
		# automate_experiment('experiments/pl_tests/pl_us-east-1', 'en', 'en', 20)
		# automate_experiment('experiments/pl_tests/pl_us-west-1', 'en', 'en', 20)
		# automate_experiment('experiments/pl_tests/pl_us-west-2', 'en', 'en', 20)

		# automate_experiment('experiments/2014.06.11/large03/large03_000', 'ew1', 'uw1', 15)
		# automate_experiment('experiments/2014.06.11/large03/large03_025', 'ew1', 'uw1', 15)
		# automate_experiment('experiments/2014.06.11/large03/large03_100', 'ue1', 'uw1', 15)
		# automate_experiment('experiments/2014.06.11/large03/large03_250', 'uw1', 'uw1', 15)

		pass
	finally:
		print 'DONE!!!'
		m = aws_manager.AWS_Manager()
		m.update_running_instances()
		m.terminate_instances()
		pass

def automate_experiment(script_name, first_engine_name, last_engine_name, num_of_tests):
	global logger

	print 'Executing workflow from script {0}'.format(script_name)
	exe = executor.Executor(script_name)

	if first_engine_name not in exe.engines:
		raise Exception('Unknow first engine {0}'.format(first_engine_name))
	if last_engine_name not in exe.engines:
		raise Exception('Unknow last engine {0}'.format(last_engine_name))

	print 'Start hosts'
	exe.start_host()
	print 'All hosts are started'

	# sleep 10 seconds to make sure that all hosts are ready
	time.sleep(10)

	try:
		print 'Start deploying services'
		exe.deploy_services()
		print 'Finish deploying services'

		try:
			print 'Start executing workflow'
			line = ''
			for i in range(num_of_tests):
				start = time.time()
				exe.init_engines()
				time.sleep(2)
				line = exe.monitor_engine_log(last_engine_name, previous_str=line)
				end = time.time()
				print 'Execution time: {0}'.format(end - start)
			print 'Finish executing workflow'

			time.sleep(5)
			log_folder = script_name if '/' not in script_name else script_name.split('/')[-1]
			# deployment.execute_command('mkdir {0}'.format(log_folder))
			subprocess.Popen(['mkdir', log_folder])
			print 'Start copying engine log files to folder {0}'.format(log_folder)
			for e in exe.engines:
				host = exe.hosts[e]
				# deployment.execute_command('scp {0}@{1}:~/engine.log {2}/{3}.log'.format(host.username, host.host, log_folder, e))
				subprocess.Popen(['scp', '{0}@{1}:~/engine.log'.format(host.username, host.host), '{0}/{1}.log'.format(log_folder, e)])
				
			print 'Finish copying engine log files to folder {0}'.format(log_folder)
			time.sleep(10)

			for e in exe.engines:
				host = exe.hosts[e]
				try:
					print "ssh {0}@{1} 'rm ~/engine.log'".format(host.username, host.host)
					subprocess.Popen(['ssh', '{0}@{1}'.format(host.username, host.host), "rm ~/engine.log"])
				except Exception:
					pass

			start_grep_lines = search_lines_in_file('{0}/{1}.log'.format(log_folder, first_engine_name), 'Start')

			end_grep_lines = search_lines_in_file('{0}/{1}.log'.format(log_folder, last_engine_name), 'time')

			# print start_grep_lines
			# print end_grep_lines
			f = open('{0}/{1}'.format(log_folder, log_folder), 'w+')
			try:
				for l in start_grep_lines:
					f.write('{0}\n'.format(l))
				for l in end_grep_lines:
					f.write('{0}\n'.format(l))
			finally:
				f.close()

			if len(start_grep_lines) == len(end_grep_lines):
				num = len(start_grep_lines)
				_times = []
				for n in range(num):
					s = datetime.datetime.strptime(start_grep_lines[n].split()[1], '%H:%M:%S,%f')
					e = datetime.datetime.strptime(end_grep_lines[n].split()[1], '%H:%M:%S,%f')
					_times.append((e - s).total_seconds())
				_times.sort()
				times = _times[:10]
				print '{0}\t{1}\t{2}\t{3}\t{4}'.format(script_name, len(exe.engines), times, stats.mean(times), stats.standard_deviation(times))
				logger.info('{0}\t{1}\t{2}\t{3}\t{4}'.format(script_name, len(exe.engines), times, stats.mean(times), stats.standard_deviation(times)))

			time.sleep(10)
		finally:

			print 'Start terminating services'
			exe.terminate_services()
			print 'Finish terminating services'
	finally:
		print 'Start stopping hosts'
		# exe.stop_hosts()
		print 'Finish stopping hosts'

def search_lines_in_file(file_name, search_term):
	for i in range(10):
		with open(file_name) as f:
			lines = f.readlines()
			result = []
			for line in lines:
				if search_term in line:
					result.append(line)
		if len(result) == 15: break
		else: time.sleep(5)
	return result

if __name__ == '__main__':
	argv = sys.argv

	if len(argv) == 1:
		automate_experiments()
		sys.exit()
	elif len(argv) != 5:
		print 'python exp_automator.py <script_file_name> <first_engine_name> <last_engine_name> <num_of_tests>'
		sys.exit()

	script_name = argv[1]
	first_engine_name = argv[2]
	last_engine_name = argv[3]
	num_of_tests = int(argv[4])
	automate_experiment(script_name, first_engine_name, last_engine_name, num_of_tests)
