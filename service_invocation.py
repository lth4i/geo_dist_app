class Service_Invocation_State:
	WAITING = "WAITING"
	INVOKING = "INVOKING"
	INVOKED = "INVOKED"

class Service_Invocation:
	def __init__(self, host, service_name, input_string, output):
		self.host = host
		self.service_name = service_name
		self.input_string = input_string
		self.output = output

		self.state = Service_Invocation_State.WAITING

		self.retry_count = 0

	def __str__(self):
		return "[{0}, {1}, {2}, {3}, {4}]".format(self.host, self.service_name,
			self.input_string, self.output, self.state)

	def __repr__(self):
		return self.__str__()