import requests
import ast
import sys
import time

def get_logs(hosts):
	streams = []
	if type(hosts) != dict: hosts = ast.literal_eval(hosts)
	for host in hosts:
		r = requests.get('http://{0}:8080/Log_Producer/{1}'.format(host, hosts[host]))
		if r.status_code != 200:
			raise Exception(str(r.text))
		else:
			streams.append(r.text)
	return streams

def find_errors(streams):
	errors = []
	for stream in streams:
		for line in stream.split():
			if 'ERROR' in line: errors.append(line)
	return errors

class Distribured_Error_Finder:
	exposed = True

	def POST(self, hosts):
		hosts = ast.literal_eval(hosts) # hosts is a dict from hostname/ip to size
		streams = get_logs(hosts)
		errors = find_errors(streams)

		return str(errors)

if __name__ == '__main__':
	if len(sys.argv) < 4:
		print 'python errors_finder.py -d distributed_host ... -s data_sources size ...'
		sys.exit()

	argv = sys.argv
	d_index = argv.index('-d')
	s_index = argv.index('-s')
	units = argv[d_index + 1:s_index]
	# host = argv[1]

	counter = s_index + 1
	hosts = {}
	total_size = 0.0
	while counter < len(argv):
		source_host = argv[counter]
		size = int(argv[counter + 1])
		total_size = total_size + size
		hosts[source_host] = size
		counter = counter + 2

	for i in range(10):
		# local find
		start = time.time()
		e = find_errors(get_logs(hosts))
		end = time.time()
		local_time = end - start

		# distributed find
		distributed_times = {}
		for host in units:
			start = time.time()
			r = requests.post('http://{0}:8080/Distribured_Error_Finder'.format(host), {'hosts':str(hosts)})
			if r.status_code != 200: raise Exception(r.text)
			end = time.time()
			distributed_time = end - start
			distributed_times[host] = distributed_time
			# print len(r.text) / total_size
		print local_time, distributed_times